// ==============================================
// Pointer abstraction
// ==============================================

/**
 * This class provides an easy and abstracted mechanism to determine the
 * best pointer behavior to use -- that is, is the user currently interacting
 * with their device in a touch manner, or using a mouse.
 *
 * Since devices may use either touch or mouse or both, there is no way to
 * know the user's preferred pointer type until they interact with the site.
 *
 * To accommodate this, this class provides a method and two events
 * to determine the user's preferred pointer type.
 *
 * - getPointer() returns the last used pointer type, or, if the user has
 *   not yet interacted with the site, falls back to a Modernizr test.
 *
 * - The mouse-detected event is triggered on the window object when the user
 *   is using a mouse pointer input, or has switched from touch to mouse input.
 *   It can be observed in this manner: $j(window).on('mouse-detected', function(event) { // custom code });
 *
 * - The touch-detected event is triggered on the window object when the user
 *   is using touch pointer input, or has switched from mouse to touch input.
 *   It can be observed in this manner: $j(window).on('touch-detected', function(event) { // custom code });
 */
var PointerManager = {
    MOUSE_POINTER_TYPE: 'mouse',
    TOUCH_POINTER_TYPE: 'touch',
    POINTER_EVENT_TIMEOUT_MS: 500,
    standardTouch: false,
    touchDetectionEvent: null,
    lastTouchType: null,
    pointerTimeout: null,
    pointerEventLock: false,

    getPointerEventsSupported: function() {
        return this.standardTouch;
    },

    getPointerEventsInputTypes: function() {
        if (window.navigator.pointerEnabled) { //IE 11+
            //return string values from http://msdn.microsoft.com/en-us/library/windows/apps/hh466130.aspx
            return {
                MOUSE: 'mouse',
                TOUCH: 'touch',
                PEN: 'pen'
            };
        } else if (window.navigator.msPointerEnabled) { //IE 10
            //return numeric values from http://msdn.microsoft.com/en-us/library/windows/apps/hh466130.aspx
            return {
                MOUSE:  0x00000004,
                TOUCH:  0x00000002,
                PEN:    0x00000003
            };
        } else { //other browsers don't support pointer events
            return {}; //return empty object
        }
    },

    /**
     * If called before init(), get best guess of input pointer type
     * using Modernizr test.
     * If called after init(), get current pointer in use.
     */
    getPointer: function() {
        // On iOS devices, always default to touch, as this.lastTouchType will intermittently return 'mouse' if
        // multiple touches are triggered in rapid succession in Safari on iOS
        if(Modernizr.ios) {
            return this.TOUCH_POINTER_TYPE;
        }

        if(this.lastTouchType) {
            return this.lastTouchType;
        }

        return Modernizr.touch ? this.TOUCH_POINTER_TYPE : this.MOUSE_POINTER_TYPE;
    },

    setPointerEventLock: function() {
        this.pointerEventLock = true;
    },
    clearPointerEventLock: function() {
        this.pointerEventLock = false;
    },
    setPointerEventLockTimeout: function() {
        var that = this;

        if(this.pointerTimeout) {
            clearTimeout(this.pointerTimeout);
        }

        this.setPointerEventLock();
        this.pointerTimeout = setTimeout(function() { that.clearPointerEventLock(); }, this.POINTER_EVENT_TIMEOUT_MS);
    },

    triggerMouseEvent: function(originalEvent) {
        if(this.lastTouchType == this.MOUSE_POINTER_TYPE) {
            return; //prevent duplicate events
        }

        this.lastTouchType = this.MOUSE_POINTER_TYPE;
        $j(window).trigger('mouse-detected', originalEvent);
    },
    triggerTouchEvent: function(originalEvent) {
        if(this.lastTouchType == this.TOUCH_POINTER_TYPE) {
            return; //prevent duplicate events
        }

        this.lastTouchType = this.TOUCH_POINTER_TYPE;
        $j(window).trigger('touch-detected', originalEvent);
    },

    initEnv: function() {
        if (window.navigator.pointerEnabled) {
            this.standardTouch = true;
            this.touchDetectionEvent = 'pointermove';
        } else if (window.navigator.msPointerEnabled) {
            this.standardTouch = true;
            this.touchDetectionEvent = 'MSPointerMove';
        } else {
            this.touchDetectionEvent = 'touchstart';
        }
    },

    wirePointerDetection: function() {
        var that = this;

        if(this.standardTouch) { //standard-based touch events. Wire only one event.
            //detect pointer event
            $j(window).on(this.touchDetectionEvent, function(e) {
                switch(e.originalEvent.pointerType) {
                    case that.getPointerEventsInputTypes().MOUSE:
                        that.triggerMouseEvent(e);
                        break;
                    case that.getPointerEventsInputTypes().TOUCH:
                    case that.getPointerEventsInputTypes().PEN:
                        // intentionally group pen and touch together
                        that.triggerTouchEvent(e);
                        break;
                }
            });
        } else { //non-standard touch events. Wire touch and mouse competing events.
            //detect first touch
            $j(window).on(this.touchDetectionEvent, function(e) {
                if(that.pointerEventLock) {
                    return;
                }

                that.setPointerEventLockTimeout();
                that.triggerTouchEvent(e);
            });

            //detect mouse usage
            $j(document).on('mouseover', function(e) {
                if(that.pointerEventLock) {
                    return;
                }

                that.setPointerEventLockTimeout();
                that.triggerMouseEvent(e);
            });
        }
    },

    init: function() {
        this.initEnv();
        this.wirePointerDetection();
    }
};

(function() {

	'use strict';

	var $ = jQuery;
	// MAIN VARIABLES INITIALIZATION
	var showMobileMenuWidth = 1200;
	var body                = $('body');
	var wrapper             = $('#wrapper');
	var toggleMenu          = $('.toggle-menu');
	var topnav              = $('.main-nav-wrapper');
	var topnavHeight        = $('#main-navigation').height();
	var topbarHeight        = $('#topbar').height() + 1;
	var headerTopHeight     = $('.header-top').height();
	var windowWidth         = $(window).width();
	var windowHeight        = $(window).height();
	var scrollPos           = $(window).scrollTop();
	var lastScrollTop       = 0;
	var fullPageCreated     = false;
	var isOpera             = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
	var isFirefox           = typeof InstallTrigger !== 'undefined';
	var isSafari            = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
	var isIE                = false || !!document.documentMode;
	var isEdge              = !isIE && !!window.StyleMedia;
	var isChrome            = !!window.chrome && !!window.chrome.webstore;
	var isBlink             = (isChrome || isOpera) && !!window.CSS;
	var mainContent         = $('#main-content');
	var toggleBtn           = $('.toggle-menu');
	var sectionHeader       = $('.section-header');
	var sectionHeaderHeight = $('.section-header').height();
	var burgerMenuOpen = false;
	var subMenuSmallOpen = false;
	var burgerSearchOpen = false;

	AOS.init();


	$(window).scroll(function () {
		scrollPos = $(window).scrollTop();
	});

	$(window).resize(function () {
		'use strict';		

		windowWidth = $(window).width();
		if (windowWidth <= 850) {
			$('body').addClass('small-device');
		}
		else {
			$('body').removeClass('small-device');	
		}
		$('body').removeClass('bmenu-open');
		burgerMenuOpen = false;

		if (windowWidth > 768) {
			$('aside .widget .widget-content').slideDown(0);
			$('aside .widget .widget-content').removeClass('open');
		}

		footerReveal();
		halfSection();
		equalHeight();
		resizeProductList();
	});

	(function () {
		'use strict';

		var isMobile = {
			Android: function() {
				return navigator.userAgent.match(/Android/i);
			},
			BlackBerry: function() {
				return navigator.userAgent.match(/BlackBerry/i);
			},
			iOS: function() {
				return navigator.userAgent.match(/iPhone|iPad|iPod/i);
			},
			Opera: function() {
				return navigator.userAgent.match(/Opera Mini/i);
			},
			Windows: function() {
				return navigator.userAgent.match(/IEMobile/i);
			},
			any: function() {
				return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
			}
		};

		
		halfSection();
		equalHeight();
		resizeProductList();
		colorBackground();
		handleSelect();
		handleQuantity();
		iconHover();
		rating();
		imageZoom();
		handleModals();

		setTimeout(function() {
			$(window).trigger('resize');
			owlCarousel();
			flexslider();
		}, 500);
		setTimeout(function() {
			footerReveal();
		}, 1000);

	}());

	// HANDLE COLORS & BACKGROUND
	function colorBackground(){
		// Background Color
		$('[data-bg-color]').each(function() {
			var bgColor = $(this).attr('data-bg-color');
			$(this).css('background-color', bgColor);
		});
		// Image Background 
		$('[data-bg-img]').each(function() {
			var dataImg = $(this).attr('data-bg-img');
			$(this).css('background-image', 'url(' + dataImg + ')');
		});
		// Pattern Background
		$('[data-bg-pattern]').each(function() {
			var dataImg = $(this).attr('data-bg-pattern');
			$(this).css('background-image', 'url(' + dataImg + ')');
		});
		// Font Color
		$('[data-color]').each(function() {
			var color = $(this).attr('data-color');
			$(this).css('color', color);
			if($(this).hasClass('icon-line')) {
				$(this).css('border-color', color);
			}
		});
	}


	// HALF SECTION 
	function halfSection() {
		$('.half-section .text-element-wrapper').css('min-height', '');
		$('.half-section .img-cover').css('height', '');
		if ($(window).width() > 992) {
			$('.half-section').removeClass('half-section-fullwidth');
			$('.half-section').each(function() {
				$(this).find('.text-element-wrapper').css('min-height', $(this).height() - 40);
				if ($(this).hasClass('half-section-first') || $(this).hasClass('half-section-last')) {
					$(this).find('.text-element-wrapper').css('min-height', $(this).height() - 20);
				}
				if ($(this).hasClass('half-section-alone')) {
					$(this).find('.text-element-wrapper').css('min-height', $(this).height());
				}
			});
		} else {
			$('.half-section').addClass('half-section-fullwidth');
			$('.half-section').find('.img-cover').height($('.img-cover').width());
		}
	}

	// FOOTER REVEAL
	function footerReveal() {
		if (body.hasClass('footer-reveal')) {
			if (windowWidth > 850) {
				var footerHeight = $('#footer').height();
				mainContent.css('margin-bottom', footerHeight);
			}
			else {
				var footerHeight;
				mainContent.css('margin-bottom', 0);
			}
		}
	}

	// EQUAL HEIGHTS
	function equalHeight() {
		// By Items
		$('[data-equal-height-items]').each(function() {
			var itemsElm = $(this).find('.item');
			itemsElm.height('');
			var maxItemH = 0;
			itemsElm.each(function () {
				if($(this).height() > maxItemH)
					maxItemH = $(this).height();
			});
			itemsElm.height(maxItemH);
		});
		// By Sub Items
		$('[data-equal-height-sub]').each(function() {
			var itemsElm = $(this).find('.sub-item');
			itemsElm.height('');
			var maxItemH = 0;
			itemsElm.each(function () {
				if($(this).height() > maxItemH)
					maxItemH = $(this).height();
			});
			itemsElm.height(maxItemH);
		});
	}

	// SELECT INPUT
	function handleSelect() {
	    if ($.fn.select2) {
	        setTimeout(function(){
	            $('select:not(.select-picker):not(.super-attribute-select):not(.product-custom-option):not(#billing-address-select):not(#country_id)').each(function() {
	                $(this).select2({
	                    placeholder: $(this).data('placeholder') ? $(this).data('placeholder') : '',
	                    allowClear: $(this).data('allowclear') ? $(this).data('allowclear') : false,
	                    minimumInputLength: $(this).data('minimumInputLength') ? $(this).data('minimumInputLength') : -1,
	                    minimumResultsForSearch: $(this).data('search') ? 1 : -1,
	                    dropdownCssClass: $(this).data('style') ? $(this).data('style') : '',
	                    containerCssClass: $(this).data('container-class') ? $(this).data('container-class') : ''
	                });
	            });
	        },200);
	    }
	    if ($('.select-picker').length && $.fn.selectpicker) {
	        $('.select-picker').selectpicker('render');
	    }
	}

	// RATING
	function rating() {
	    $('.rating').each(function() {
	        $(this).raty({
	            cancel: false,
	            // Creates a cancel button to cancel the rating.
	            cancelClass: 'raty-cancel',
	            // Name of cancel's class.
	            cancelHint: 'Cancel this rating!',
	            // The cancel's button hint.
	            cancelOff: 'cancel-off.png',
	            // Icon used on active cancel.
	            cancelOn: 'cancel-on.png',
	            // Icon used inactive cancel.
	            cancelPlace: 'left',
	            // Cancel's button position.
	            half: false,
	            // Enables half star selection.
	            halfShow: true,
	            // Enables half star display.
	            hints: [
	                '',
	                '',
	                '',
	                '',
	                ''
	            ],
	            // Hints used on each star.
	            noRatedMsg: '',
	            // Hint for no rated elements when it's readOnly.
	            number: 5,
	            // Number of stars that will be presented.
	            numberMax: 20,
	            // Max of star the option number can creates.
	            precision: false,
	            // Enables the selection of a precision score.
	            readOnly: $(this).data('read-only') ? $(this).data('read-only') : false,
	            // Turns the rating read-only.
	            round: {
	                down: 0.25,
	                full: 0.6,
	                up: 0.76
	            },
	            // Included values attributes to do the score round math.
	            score: $(this).data('value') ? $(this).data('value') : undefined,
	            // Initial rating.
	            scoreName: 'ratings[1]',
	            // Name of the hidden field that holds the score value.
	            single: false,
	            // Enables just a single star selection.
	            space: true,
	            // Puts space between the icons.
	            starHalf: 'fa fa-star-half-o',
	            // The name of the half star image.
	            starOff: 'fa fa-star-o',
	            // Name of the star image off.
	            starOn: 'fa fa-star',
	            // Name of the star image on.
	            targetText: '',
	            // Default text setted on target.
	            targetType: 'hint',
	            // Option to choose if target will receive hint o 'score' type.
	            starType: 'i',
	            click: function() {}
	        });
	    });
	}

	// QUANTITY INPUT
	function handleQuantity() {
	    $('body').on('click', '.quantity .plus', function() {
	        var currentVal = parseInt($(this).parent().find('input').val());
	        $(this).parent().find('input').val(currentVal + 1);
	    });
	    $('body').on('click', '.quantity .minus', function() {
	        var currentVal = parseInt($(this).parent().find('input').val());
	        if (currentVal > 1) {
	            $(this).parent().find('input').val(currentVal - 1);
	        }
	    });
	}

	// Defult Option Selection
	// $(".configurable-swatch-list li:first-child a").each(function(){
	// 	$(this).children("span").trigger("click");
	// 	$(this).children("span").click();
	// });
	
	var curOptOpen = false;
	$('ul.configurable-swatch-list').each(function() {
		var parentUl = $(this);
		var parentAttr = parentUl.closest('.swatch-attr');
		var childLi = parentUl.find('li');
		var baseLabel = parentUl.find('.selected-label');
		childLi.on('click', function(event) {
			var elChosen = false;
			event.stopPropagation();
			
			//parentAttr.addClass('open');
			curOptOpen = parentAttr;

			if (!parentAttr.hasClass('open')) {
				parentAttr.addClass('open');
			}
			else {
				parentAttr.removeClass('open');	
			}

			$('dd.swatch-attr').not(curOptOpen).removeClass('open');

			//$('dd.swatch-attr').removeClass('open');
			
			// if (parentAttr.hasClass('open') && event.target.closest('.selected-label')) {
			// 	parentAttr.removeClass('open');	
			// }
			//console.log(event.target.closest('.selected-label'));
			//$('ul.configurable-swatch-list').removeClass('open');
			// if (!parentAttr.hasClass('open')) {
			// 	parentAttr.addClass('open');
			// }
			// else {
			// 	parentAttr.removeClass('open');	
			// }
		});

		childLi.find('a').on('click', function(event) {
			baseLabel.find('em').addClass('base');
			if ($(this).parent('li').hasClass('not-available')) {
				baseLabel.find('em').addClass('not-available');
			}
			else {
				baseLabel.find('em').removeClass('not-available');
			}
			baseLabel.find('em').text($(this).text());
			parentUl.parent().find('.validation-advice').fadeOut(250);
			parentAttr.removeClass('validation-failed');
			parentAttr.removeClass('open');
//			var parentLi = $(this).parent('li');
//			parentLi.trigger('click');
		});

	});

	$(document).on('click', function(event) {
		if ($('dd.swatch-attr').length > 0) {
			$('dd.swatch-attr').removeClass('open');
			return;
		}
	});

	// SOCIAL ICONS HOVER EFFECT 
	function iconHover() {
	    $('.icon-hover a').each(function() {
	        var icon = $(this).html();
	        $(this).append(icon);
	    });
	}

	// SHOP IMAGE ZOOM EFFECT
	function imageZoom() {
	    if($('.easyzoom').length){
	        var $easyzoom = $('.easyzoom').easyZoom();
	    }   
	}

	
	$('#review-form').submit(function() {
		$('#summary_field').val($('#review_field').val());
	});

	// OWL CAROUSEL
	function owlCarousel() {
	  $('[data-plugin-carousel]:not(.manual), .owl-carousel:not(.manual)').each(function () {
	    var $carousel = $(this), opts = null, pluginOptions = $carousel.data('plugin-options'), defaults = {
	        'autoplay': false,
	        'autoplayTimeout': 4000,
	        'autoplayHoverPause': true,
	        'loop': false,
	        'dots': false,
	        'margin': 10,
	        'nav': true,
	        'center': false,
	        'startPosition': 0,
	        'items': 5,
	        'navRewind': false,
	        responsive: {
	          0: { items: $carousel.data('items-mobile') ? $carousel.data('items-mobile') : 1 },
	          768: { items: $carousel.data('items-tablet') ? $carousel.data('items-tablet') : 3 },
	          1024: { items: $carousel.data('items-desktop') ? $carousel.data('items-desktop') : 5 }
	        },
	        'navText': [
	          '<i class="nc-icon-outline arrows-1_minimal-left"></i>',
	          '<i class="nc-icon-outline arrows-1_minimal-right"></i>'
	        ]
	      };
	    opts = $.extend({}, defaults, pluginOptions);
	    $carousel.owlCarousel(opts);
	    function owlNav() {
	      var carouselImageHeight = $carousel.find('figure').height();
	      if (carouselImageHeight === null) {
	        return;
	      }
	      if ($carousel.hasClass('product-carousel')) {
	        var itemHeight = $carousel.find('.item').height();
	        $carousel.find('.owl-prev, .owl-next').css('top', itemHeight / 2);
	      } else if ($carousel.hasClass('features-carousel')) {
	        var itemHeight = $carousel.find('.item').height();
	        $carousel.find('.owl-prev, .owl-next').css('top', itemHeight / 2);
	      } else {
	        var navPos = carouselImageHeight / 2;
	        $carousel.find('.owl-prev, .owl-next').css('top', carouselImageHeight / 2);
	      }
	      $carousel.on('changed.owl.carousel', function (event) {
	        $carousel.find('figure').addClass(' visible');
	      });
	    }
	    owlNav();
	    $carousel.on('resized.owl.carousel', function (event) {
	      owlNav();
	    });
	  });
	}

	// FLEXSLIDER
	function flexslider() {
	  $('[data-plugin-flexslider]:not(.manual), .flexslider:not(.manual)').each(function () {
	    var $flexsl = $(this), opts = null, pluginOptions = $flexsl.data('plugin-options'), defaults = {
	        'animation': 'fade', //String: Select your animation type, "fade" or "slide"
	        'easing': 'swing',//{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
	        'direction': 'horizontal', //String: Select the sliding direction, "horizontal" or "vertical"
	        'animationLoop': true, //Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
	        'smoothHeight': false, //{NEW} Boolean: Allow height of the slider to animate smoothly in horizontal mode  
	        'startAt': 0, //Integer: The slide that the slider should start on. Array notation (0 = first slide)
	        'slideshow': true, //Boolean: Animate slider automatically
	        'slideshowSpeed': 7000,//Integer: Set the speed of the slideshow cycling, in milliseconds
	        'animationSpeed': 600,//Integer: Set the speed of animations, in milliseconds
	        'initDelay': 0, //{NEW} Integer: Set an initialization delay, in milliseconds
	        'pauseOnAction': true,  //Boolean: Pause the slideshow when interacting with control elements, highly recommended.
	        'pauseOnHover': true, //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
	        'touch': true, //{NEW} Boolean: Allow touch swipe navigation of the slider on touch-enabled devices
	        'video': false, //{NEW} Boolean: If using video in the slider, will prevent CSS3 3D Transforms to avoid graphical glitches
	        'controlNav': true, //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
	        'directionNav': true, //Boolean: Create navigation for previous/next navigation? (true/false)
	        'prevText': '', //String: Set the text for the "previous" directionNav item
	        'nextText': '',//String: Set the text for the "next" directionNav item
	        'keyboard': true, //Boolean: Allow slider navigating via keyboard left/right keys
	        'multipleKeyboard': false, //{NEW} Boolean: Allow keyboard navigation to affect multiple sliders. Default behavior cuts out keyboard navigation with more than one slider present.
	        'mousewheel': false,  //{UPDATED} Boolean: Requires jquery.mousewheel.js (https://github.com/brandonaaron/jquery-mousewheel) - Allows slider navigating via mousewheel
	        'pausePlay': false, //Boolean: Create pause/play dynamic element
	        'pauseText': 'Pause', //String: Set the text for the "pause" pausePlay item
	        'playText': 'Play',  //String: Set the text for the "play" pausePlay item
	        'controlsContainer': '', //{UPDATED} Selector: USE CLASS SELECTOR. Declare which container the navigation elements should be appended too. Default container is the FlexSlider element. Example use would be ".flexslider-container". Property is ignored if given element is not found.
	        'manualControls': '', //Selector: Declare custom control navigation. Examples would be ".flex-control-nav li" or "#tabs-nav li img", etc. The number of elements in your controlNav should match the number of slides/tabs.
	        'itemWidth': 0,  //{NEW} Integer: Box-model width of individual carousel items, including horizontal borders and padding.
	        'itemMargin': 0, //{NEW} Integer: Margin between carousel items.
	        'minItems': 0,//{NEW} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
	        'maxItems': 0
	      };
	    opts = $.extend({}, defaults, pluginOptions);
	    $flexsl.flexslider(opts);
	  });
	}

	if ($.fn.flexslider) {
        $('#product-slider-thumbnails').flexslider({
            animation: 'slide',
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemMargin: 10,
            itemWidth: 120,
            'prevText': '',
            'nextText': '',
            asNavFor: '#product-slider'
        });
        $('#product-slider').flexslider({
            animation: 'fade',
            controlNav: false,
            directionNav: false,
            animationLoop: false,
            slideshow: false,
            'prevText': '',
            'nextText': ''
        });
    }

	$('.go-tab').on('click', function(e) {
        e.preventDefault();
        var target = this.href.split('#');
	    $('.nav-item a').filter('a[href="#'+target[1]+'"]').tab('show');
        

        $('html, body').animate({
            scrollTop: $('.product-info').offset().top
        }, 1000);
    });

    // MODAL WINDOWS 
	function handleModals() {
	    if ($.fn.slickModals) {
	        $('.slickModal').each(function() {
	            var $modalSlick = $(this),
	                opts = null,
	                pluginOptions = $modalSlick.data('plugin-options'),
	                defaults = {
	                    'popupType': '',
	                    'delayTime': 2500,
	                    'exitTopDistance': 40,
	                    'scrollTopDistance': 400,
	                    'setCookie': false,
	                    'cookieDays': 0,
	                    'cookieTriggerClass': 'setCookie-1',
	                    'cookieName': 'slickModal-1',
	                    'overlayBg': true,
	                    'overlayBgColor': 'rgba(0,0,0,0.9)',
	                    'overlayTransition': 'ease',
	                    'overlayTransitionSpeed': '0.4',
	                    'bgEffect': null,
	                    'blurBgRadius': '2px',
	                    'scaleBgValue': '0.9',
	                    'windowWidth': '500px',
	                    'windowHeight': '500px',
	                    'windowLocation': 'center',
	                    'windowTransition': 'ease',
	                    'windowTransitionSpeed': '0.4',
	                    'windowTransitionEffect': 'slideLeft',
	                    'windowShadowOffsetX': '0',
	                    'windowShadowOffsetY': '0',
	                    'windowShadowBlurRadius': '0',
	                    'windowShadowSpreadRadius': '0',
	                    'windowShadowColor': 'rgba(0,0,0,0)',
	                    'windowBackground': 'rgba(255,255,255,0)',
	                    'windowRadius': '0',
	                    'windowMargin': 'auto',
	                    'windowPadding': '0',
	                    'closeButton': 'text', 
	                    'reopenClass': 'slick-modal'
	                };
	            opts = $.extend({}, defaults, pluginOptions);
	            $modalSlick.slickModals(opts);
	        });
	    }
	}

	var editorLoaded = false;
	var editorFinition = 'attribute133';
	var editorFinitionSelect = '';
	var lastFinition = '';

	$('#perso_tool_btn').on('click', function(e) {
		e.preventDefault();
	    var formToValidate = document.getElementById('product_addtocart_form');
	    var validator = new Validation(formToValidate);
	    if(validator.validate()) {
	    	editorFinitionSelect = $('#'+editorFinition).val();
	    	if (lastFinition != editorFinitionSelect && lastFinition != '') {
	    		editorLoaded = false;
	    	}
	    	lastFinition = editorFinitionSelect;
	    	if (editorFinitionSelect != '') {
	    		$('body').addClass('editor-open');
	    	}
	    }
	});




	$('#close_editor_toggle').on('click', function() {
		var elmWarning = document.createElement("div");
		elmWarning.innerHTML = editorResetText;
		swal({
			title: editorResetTitle,
			content: elmWarning,
			icon: "warning",
			buttons: [cancelButtonText, confirmButtonText],
			dangerMode: true,
		})
		.then((reload) => {
			if (reload) {
				location.reload();
			}
		});
	});
	
	$('.choice-design-type').on('click', function() {
		
		var dataType = $(this).attr('data-type');
		var dataSku = $(this).attr('data-sku');
		$('.step-1').fadeOut(250, function() {
			$('#overlay_customize_box').removeClass('product-design');
			if (dataType == 'blank') {
				//var optionVal = $('#'+typeFlag).val();
				if (dataSku) {
					if (editorLoaded == false) {
						$.ajax({
				            url : productEditorUrl+'option_id/'+dataSku,
				            type : 'get',
				            success : function( response ) {
				            	$('#overlay_customize_box').addClass('product-design');
				                $('#editor_area').html(response);
				                launchEditor();
				                $('.type-'+dataType).fadeIn(250);
				                editorLoaded = true;
				            },
				            error: function(error) {
				            	
				            }
				        });
				    }
				    else {
				    	$('.type-'+dataType).fadeIn(250);	
				    }
				}
			}
			else {
				$('.type-'+dataType).fadeIn(250);
			}
		});
	});

	
	$('.back-link').on('click', function() {
		$('.substep').fadeOut(250, function() {
			setTimeout(function() {
				$('.step-1').fadeIn(250);
				if (typeof dropDesign !== "undefined") {
					dropDesign.removeAllFiles();
				}
			}, 250);
		});
	});

	var yourDesigner;
	var initialZoomDesigner = 1;
	var designerBloc;
	function launchEditor() {
		designerBloc = $('#flag_designer');
    		var pluginOpts = {
	    		stageWidth: 1200,
	    		editorMode: false,
	    		templatesDirectory: "/",
	    		templatesType: 'html',
	    		langJSON: langEditorUrl,
	    		hideDialogOnAdd: true,
	    		draggable: true,
				toolbarPlacement: "inside-top",
				fitImagesInCanvas: true,
				inCanvasTextEditing: true,
	    		fonts: [
			    	{name: 'Helvetica'},
			    	{name: 'Times New Roman'},
			    	{name: 'Arial'},
		    		{name: 'Alegreya', url: 'google'},
		    		{name: 'Roboto regular', url: 'google'},
		    		{name: 'Kreon', url: 'google'},
		    		{name: 'Pattaya', url: 'google'},
		    		{name: 'Belleza', url: 'google'},
		    		{name: 'Philosopher', url: 'google'},
		    		{name: 'Merriweather', url: 'google'},
		    		{name: 'Alice', url: 'google'},
		    		{name: 'Cormorant', url: 'google'},
		    		{name: 'Geramond', url: 'google'},
		    		{name: 'Graduate', url: 'google'},
		    		{name: 'Alex Brush', url: 'google'},
		    		{name: 'Special Elite', url: 'google'},
		    		{name: 'Pacifico', url: 'google'},
		    		{name: 'Chewy', url: 'google'},
		    		{name: 'Schoolbell', url: 'google'},
		    		{name: 'Source Code Po', url: 'google'},
		    		{name: 'Shrikhand', url: 'google'},
		    		{name: 'Sacramento', url: 'google'},
		    		{name: 'Monoton', url: 'google'}
		    	],
	    		customTextParameters: {
		    		colors: true,
		    		removable: true,
		    		resizable: true,
		    		draggable: true,
		    		rotatable: true,
		    		autoCenter: true,
		    		boundingBox: "Base",
		    		boundingBoxMode: 'inside',
		    		zChangeable: true
		    	},
	    		customImageParameters: {
	    			minW: 800, // 1920
	    			minH: 600, // 1080
	    			maxW: 3000,
					maxH: 3000,
		    		draggable: true,
		    		removable: true,
		    		resizable: true,
		    		rotatable: true,
		    		colors: '#000',
		    		autoCenter: true,
		    		boundingBox: "Base",
		    		boundingBoxMode: 'inside'
		    	},
		    	textParameters: {
		    		fontSize: 28,
		    		maxFontSize: 350
		    	},
		    	customImageAjaxSettings: {
		    		url: editorUploadUrl,
		    		data: {
				        saveOnServer: 1,
						uploadsDir: 'EDITOR'
					}
		    	},
		    	mainBarModules: ['images', 'text', 'designs', 'manage-layers'],
		    	actions:  {
					'top': [],
					'right': ['zoomin', 'zoomout', 'reset-product'],
					'bottom': ['undo','redo'],
					'left': []
				}
    		};

	    yourDesigner = new FancyProductDesigner(designerBloc, pluginOpts);

	    designerBloc.on('productCreate', function() {
	    	yourDesigner.setZoom(1);
	    	var designLeft = $('#editor_area').offset().left;
	    	var designTop = $('#editor_area').offset().top;
	    	$('.fpd-draggable-dialog').css({
				left:15,
				top: 60,
				height: $('#flag_designer').height()-65
			});
	    	$('#editor_area').append($('.fpd-draggable-dialog'));
	    });

	    designerBloc.on('beforeElementAdd', function(obj) {
	    	//console.log(obj);
	    	
	    	//yourDesigner.setElementParameters({width: 20}, fabricImage, false);
	    });

	    //$(viewInstance).on('beforeElementAdd', function(evt, type, source, title, params) {

	   


	    //you can listen to events
	    // designerBloc.on('elementAdd', function(element, el) {
	    // 	console.log(element);
	    // });
	}

	$(window).resize(function () {
		'use strict';		
		if ($('#flag_designer').length > 0) {
			$('.fpd-draggable-dialog').css({
				height: $('#flag_designer').height()-65
			});
		}
	});



	$(document).on('click', '.fpd-action-btn', function() {
		if ($(this).find('.fpd-icon-zoom-in').length > 0) {
			designZoom();
		}
		if ($(this).find('.fpd-icon-magnify').length > 0) {
			designDeZoom();
		}
		if ($(this).find('.fpd-icon-drag').length > 0) {
			designDrag();
		}
	});

	function designZoom() {
		var startVal = yourDesigner.currentViewInstance.stage.getZoom() / yourDesigner.currentViewInstance.responsiveScale;
		var zoomTo = startVal+yourDesigner.mainOptions.zoomStep;
		zoomTo = zoomTo >= yourDesigner.mainOptions ? yourDesigner.mainOptions : zoomTo;
		yourDesigner.setZoom(zoomTo);
	}
	function designDeZoom() {
		var startVal = yourDesigner.currentViewInstance.stage.getZoom() / yourDesigner.currentViewInstance.responsiveScale;
		var zoomTo = startVal-yourDesigner.mainOptions.zoomStep;
		zoomTo = zoomTo <= 1 ? 1 : zoomTo;
		yourDesigner.setZoom(zoomTo);
	}
	function designDrag() {
		if (designerBloc.hasClass('fpd-drag')) {
			yourDesigner.currentViewInstance.dragStage = false;
			designerBloc.removeClass('fpd-drag');
		}
		else {
			yourDesigner.currentViewInstance.dragStage = true;
			designerBloc.addClass('fpd-drag');
		}
		
	}

	function generateImageEditor(btnId) {
		yourDesigner.deselectElement();
	    yourDesigner.currentViewInstance.stage.setDimensions({
	        width: yourDesigner.currentViewInstance.options.stageWidth,
	        height: yourDesigner.currentViewInstance.options.stageHeight
	    });
	    //yourDesigner.currentViewInstance.stage.setZoom(1);
	    var boundingObj = yourDesigner.getElementByTitle('Base'); 
	    var myGroup = new fabric.Group();
	    myGroup.set({
	        originX: 'center',
	        originY: 'center'
	    });

	    var fabricElement = yourDesigner.getElementByTitle('Personnalisation');
	    fabricElement.visible = false;

	    var i = yourDesigner.currentViewInstance.stage.getObjects().length;

	    while (i--) {
	        var clone = fabric.util.object.clone(yourDesigner.currentViewInstance.stage.item(i));
	        clone.title = clone.title + 'clone';
	        myGroup.addWithUpdate(clone).setCoords();
	        clone.sendToBack;
	    }

	    yourDesigner.currentViewInstance.stage.add(myGroup);
	    myGroup.sendToBack();
	    yourDesigner.currentViewInstance.stage.renderAll();
	    if (typeof boundingObj !== 'undefined') {
	        var boundingRectangle = boundingObj.getBoundingRect();
	    } else {
	        var i = yourDesigner.currentViewInstance.stage.getObjects().length;
	        while (--i) {
	            var objType = yourDesigner.currentViewInstance.stage.item(i).type;
	            if (objType == 'group') {
	                var boundingRectangle = yourDesigner.currentViewInstance.stage.item(i).getBoundingRect();
	            }
	        }
	    }

	    var dataURL = yourDesigner.currentViewInstance.stage.toDataURL({
	        format: 'png',
	        multiplier: 2,
	        left: boundingRectangle.left,
	        top: boundingRectangle.top,
	        width: boundingRectangle.width,
	        height: boundingRectangle.height,
	        backgroundColor: 'transparent'
	    });
	    fabricElement.visible = true;
	    yourDesigner.currentViewInstance.stage.remove(myGroup);
	    // Call Saving
	 	$.ajax({
            url : productEditorSave,
            type : 'post',
            dataType: 'json',
            data : {
                baseImage: dataURL,
                cFlag: cpFlag,
                btnId: btnId
            },
            success : function( response ) {
				if(response.errors) {
					alert(response.errors);
				}
				else {
					if (!response.success) {
						alert(uploadErrorTxt);
					}
					// Success
					else {
						if (btnId == 'save_btn') {
							var wind = window.open(response.success, '_blank');
							wind.focus();
						}
						else {
							// Custom Option Populate
							var optionConfig = {};
							optionConfig.type = 'EDITOR';
							optionConfig.image = response.success;
							optionConfig.designer = yourDesigner.getProduct();
							optionConfig = JSON.stringify(optionConfig);
							$('.product-custom-option').val(optionConfig);
							$('#product_addtocart_form').submit();
						}
					}
				}
            	
            },
            error: function(error) {
            	alert(uploadErrorTxt);
            }
        });
	}

	//print button
	$(document).on('click', '.btn-design-saving', function(event) {
		event.preventDefault();
		var curIdBtn = $(this).attr('id');
		if (yourDesigner.getCustomElements().length <= 0) {
			swal(
				editorResetTitle,
				editorEmptyText,
				'error'
			);
			return false;
		}
		var outElms = false;
		for(var i=0; i < yourDesigner.viewInstances.length; ++i) {
			var viewElements = yourDesigner.viewInstances[i].stage.getObjects();
			for(var j=0; j < viewElements.length; ++j) {
				var element = viewElements[j];
				if (element.isOut === true) {
					outElms = true;
				}
			}
		}
		if (outElms) {
			swal({
				title: editorResetTitle,
				text: outWarningText,
				icon: "warning",
				buttons: [cancelButtonText, confirmButtonText],
			})
			.then((valid) => {
				if (valid) {
					generateImageEditor(curIdBtn);
				}
			});
		}
		else {
			generateImageEditor(curIdBtn);
		}
		return false;
	});


	if (typeof Dropzone !== "undefined" && $('#tpl_drop').length > 0) {
		Dropzone.autoDiscover = false;
		var dropDesign = new Dropzone("#file_drop_design", {
            //previewsContainer: '.dz-preview',
            maxFiles: 1,
            maxFilesize: 2,
            uploadMultiple: false,
            acceptedFiles: '.jpg,.jpeg,.pdf,.ai,.eps,.png',
            dictInvalidFileType: "dd",
            clickable: ".dz-message",
            addRemoveLinks: true,
            dictRemoveFile: '',
            dictCancelUpload: '',
            previewTemplate: document.querySelector('#tpl_drop').innerHTML,
        });
		dropDesign.on("addedfile", function(file) {
			$('#file_name_drop span').text(file.name);
		});
		dropDesign.on("error", function(file, errorMessage) {
			alert(gleUploadError);
			dropDesign.removeFile(file);
		});
		dropDesign.on("success", function(file, response) {
			response = $.parseJSON(response);
			if(response.errors) {
				alert(response.errors);
				dropDesign.removeFile(file);
			}
			else {
				if (!response.success) {
					alert(uploadErrorTxt);
					dropDesign.removeFile(file);
				}
				// Success
				else {
					var optionConfig = {};
					optionConfig.type = 'UPLOAD';
					optionConfig.image = response.success;
					optionConfig = JSON.stringify(optionConfig);
					$('.product-custom-option').val(optionConfig);
				}
			}
		});
	}


	if ($('#cart_container_area').length > 0) {
    	var baseST = parsePrice($('.cart-subtotal-val span').text());
    	shippingCost();
    	$('input[name=estimate_method]').click(function() {
	        // Ajax Recalculate
	        $('.loader-wrapper').removeClass('loaded');
	        $.ajax({
	            url : checkoutEstimateShippingUrl,
	            type : 'post',
	            dataType: 'json',
	            data : {
	                sMethod: $('input[name=estimate_method]:checked').val()
	            },
	            success : function( response ) {
	            	shippingCost();
	            	$('.loader-wrapper').addClass('loaded');
	            },
	            error: function(error) {
	            	shippingCost();
	            	alert(gleErrorShipping);
	            	$('.loader-wrapper').addClass('loaded');
	            }
	        });
	    });
        function shippingCost() {
        	if ($('input[name=estimate_method]:checked').length > 0) {
	        	var shippingCostVal = parsePrice($('input[name=estimate_method]:checked').attr('data-price'));
	            var total = baseST + shippingCostVal;
	            if ($('.cart-discount-val').length > 0) {
	        		var discount = parsePrice($('.cart-discount-val span').text());
	        		total += discount;
	        	}
	            $('.cart-total-val').html(formatPrice(total.toFixed(2)));
	        }
        }
    }

    function parsePrice(str) {
    	str = str.replace(' ', '');
    	str = str.replace(',', '.');
    	str = parseFloat(str);
    	return str;
    }

    function formatPrice(str) {
    	//str = parseFloat(str).toLocaleString('fr');
    	//str = str.replace('.', ',');
    	return formatCurrency(str, checkoutPriceFormat)

    	
    	return str;
    }

    $('#toggle-coupon a').click(function(e) {
        e.preventDefault();
        $('#aw-onestepcheckout-review-coupon').slideToggle(300).toggleClass('coupon-visible');
    });
    $('#toggle-login a').click(function(e) {
        e.preventDefault();
        $('#aw-onestepcheckout-authentification').slideToggle(300).toggleClass('login-visible');
    });


    $('#shop .product-single #info h2').each(function() {
	    var $this = $(this);
	    $this.html($this.html().replace(/(\S+)\s*$/, '<span class="foo">$1</span>'));
	});

	function resizeProductList() {
		if ($('.products-list .item').length > 0 && $('.upsell-product-single').length < 1) {
			var plTitleMaxH = 0;
			var plBlockMaxH = 0;
			$('.products-list .item .product-item-name a').height('');
			$('.products-list .item .product-item-details').height('');
			$('.products-list .item').each(function() {
				var plTitle = $(this).find('.product-item-name a');
				plTitle.each(function() {
					var plTitleH = $(this).height();
					if (plTitleH > plTitleMaxH) {
						plTitleMaxH = plTitleH;
					}
				});
				
				var pBloc = $(this).find('.product-item-details');
				pBloc.each(function() {
					var plBlockH = $(this).height();
					if (plBlockH > plBlockMaxH) {
						plBlockMaxH = plBlockH;
					}
				});
			});
			$('.products-list .item .product-item-name a').height(plTitleMaxH);
			$('.products-list .item .product-item-details').height(plBlockMaxH);
		}
	}

	if ($('.amshopby-overlay').length > 0) {
		$('.pagination-wrapper a').on('click', function() {
			$('.amshopby-overlay').addClass('visible');
            $('html, body').animate({scrollTop: 0},650);
		});
		$('.select-filters .select-filter').on('change', function() {
			$('.amshopby-overlay').addClass('visible');
            $('html, body').animate({scrollTop: 0},650);
		});
	}

	if ($('.preview-comp-editor').length > 0) {
		$('.preview-comp-editor').find('img').each(function() {
			var iW = $(this).width();
			var iH = $(this).height();
			$(this).next('.bg').width(iW).height(iH);
		});
	}

	$('#burger_menu_trigger').on('touchstart, click', function(e) {
		e.preventDefault();
		if (!burgerMenuOpen) {
			$('body').addClass('bmenu-open');
			burgerMenuOpen = true;
		}
		else {
			$('body').removeClass('bmenu-open');
			burgerMenuOpen = false;
		}
		return false;
	});
	
	$('#main-menu .top-level-menu').on('touchstart', function(e) {
		e.preventDefault();
		var closeSelf = false;
		if ($(this).parent().hasClass('open')) {
			closeSelf = true;
		}
		$('#main-menu li').removeClass('open');
		if (closeSelf) {
			$(this).parent().removeClass('open');
		}
		else {
			$(this).parent().addClass('open');
		}
		return false;
	});

	$('#search_trigger_sm').on('touchstart, click', function(e) {
		e.preventDefault();
		if (!burgerSearchOpen) {
			$('body').addClass('bsearch-open');
			burgerSearchOpen = true;
		}
		else {
			$('body').removeClass('bsearch-open');
			burgerSearchOpen = false;
		}
		return false;
	});

	$('#close_bmenu').on('touchstart, click', function(e) {
		e.preventDefault();
		$('body').removeClass('bmenu-open');
		burgerMenuOpen = false;
		return false;
	});

	if (windowWidth <= 768) {
		$('aside .widget .widget-content').slideUp(0);
	}
	$('aside .widget .widget-title').on('touchstart, click', function(e) {
		e.preventDefault();
		if (windowWidth <= 768) {
			var parentWG = $(this).parent('.widget');
			parentWG.find('.widget-content').slideToggle(250);
			parentWG.toggleClass('open');
		}
		return false;
	});


	if ($("a.help-btn-custom").length > 0) {
		$("a.help-btn-custom").fancybox({
			autoPlay: false,
			afterShow: function( instance, slide ) {
		      // After the show-slide-animation has ended - play the vide in the current slide
		      setTimeout(function() {
					slide.$slide.find('video').each(function() {
				      	$(this).trigger('pause');
				      	$(this).load();
				    });
				}, 200);
		    }
		});
	}
	

}());