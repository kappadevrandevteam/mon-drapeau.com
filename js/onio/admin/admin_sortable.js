document.addEventListener('DOMContentLoaded', function() {

	new RowSorter('table[attr-table=onio_menu_table]', {
	    handler: 'td.sorter img',
	    stickFirstRow : true,
	    stickLastRow  : false,
	    onDragStart: function(tbody, row, index)
	    {
	        console.log('start index is ' + index);
	    },
	    onDrop: function(tbody, row, new_index, old_index)
	    {
	        console.log('sorted from ' + old_index + ' to ' + new_index);
	    }
	});



}, false);