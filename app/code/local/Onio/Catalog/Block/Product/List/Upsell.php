<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Catalog
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Catalog_Block_Product_List_Upsell extends Mage_Catalog_Block_Product_List_Upsell
{


    protected function _prepareData()
    {
        $product = Mage::registry('product');
        /* @var $product Mage_Catalog_Model_Product */
        $this->_itemCollection = $product->getUpSellProductCollection()
            ->setPositionOrder()
            ->addStoreFilter()
        ;

        if (count($this->_itemCollection)) {
            return parent::_prepareData();
        }


    	$productCategories = $product->getCategoryIds();
    	
    	foreach ($productCategories as $_cat) {
			$this->_itemCollection = Mage::getModel('catalog/category')->load($_cat)
			 ->getProductCollection()
			 ->addAttributeToSelect('*') // add all attributes - optional
			 ->addAttributeToFilter('sku', array('nin' => array($product->getSku())))
			 ->addAttributeToFilter('status', 1) // enabled
			 ->addAttributeToFilter('visibility', 4);
			$this->_itemCollection->getSelect()->order(new Zend_Db_Expr('RAND()'));
    		break;
    	}


        if (Mage::helper('catalog')->isModuleEnabled('Mage_Checkout')) {
            Mage::getResourceSingleton('checkout/cart')->addExcludeProductFilter($this->_itemCollection,
                Mage::getSingleton('checkout/session')->getQuoteId()
            );

            $this->_addProductAttributesAndPrices($this->_itemCollection);
        }

        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($this->_itemCollection);

        if ($this->getItemLimit('upsell') > 0) {
            $this->_itemCollection->setPageSize($this->getItemLimit('upsell'));
        }

        $this->_itemCollection->load();

        /**
         * Updating collection with desired items
         */
        Mage::dispatchEvent('catalog_product_upsell', array(
            'product'       => $product,
            'collection'    => $this->_itemCollection,
            'limit'         => $this->getItemLimit()
        ));

        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }

        return $this;
    }

}