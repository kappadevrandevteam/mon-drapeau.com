<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Catalog
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Catalog_Block_Page_Html_Pager extends Mage_Page_Block_Html_Pager
{

	protected $_isAmastyContext = false;

	protected function _construct()
    {
        parent::_construct();
        $currentUrl = Mage::helper('core/url')->getCurrentUrl();
		$url = Mage::getSingleton('core/url')->parseUrl($currentUrl);
		$path = $url->getPath();
		$amastyKey = Mage::getStoreConfig('amshopby/seo/key');
		if ($path && preg_match('/'.$amastyKey.'/', $path)) {
			$path = str_replace('/', '', $path);
			$this->_isAmastyContext = $path;
		}
    }

	public function getPagerUrl($params=array())
    {
        $urlParams = array();
        $urlParams['_current']  = true;
        $urlParams['_escape']   = true;
        $urlParams['_use_rewrite']   = true;
        $urlParams['_query']    = $params;
        if ($this->_isAmastyContext) {
        	return $this->getUrl($this->_isAmastyContext, $urlParams);
        }
        return $this->getUrl('*/*/*', $urlParams);
    }

}