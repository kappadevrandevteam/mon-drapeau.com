<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Catalog
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Catalog_Model_Observer
{
    public function zeroproducts(Varien_Event_Observer $observer)
    {
        $currentCategory = Mage::registry('current_category');
        $currentProduct = Mage::registry('current_product');

        if($currentCategory && !$currentProduct) {
            if (count($currentCategory->getProductCollection()) <= 0) {
                $layout = $observer->getEvent()->getLayout();
                $layout->getUpdate()->addHandle('ONIO_PRODUCT_EMPTY');
            }
        }
        if ($currentProduct) {
            if (Mage::helper('onio_catalog')->isCustomProduct($currentProduct)) {
                $layout = $observer->getEvent()->getLayout();
                $layout->getUpdate()->addHandle('CUSTOM_EDITOR_PRODUCT');
            }
        }
    }

}