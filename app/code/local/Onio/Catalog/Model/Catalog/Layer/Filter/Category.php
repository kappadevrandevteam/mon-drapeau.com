<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Catalog
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Catalog_Model_Catalog_Layer_Filter_Category extends Amasty_Shopby_Model_Catalog_Layer_Filter_Category
{

	protected function prepareChildrenCollection($parentId)
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $categories */
        $categories = Mage::getModel('catalog/category')->getCollection();
        $categories->addAttributeToSelect('name');
        $categories->addAttributeToSelect('filter_label');
        $categories->addAttributeToSelect('is_anchor');
        $categories->addAttributeToFilter('parent_id', $parentId);
        $categories->addAttributeToFilter('is_active', 1);
        $categories->setOrder('position', 'asc');

        $this->addCounts($categories);
        return $categories;
    }

	protected function _prepareItemData(Mage_Catalog_Model_Category $category, $level = 1)
    {
        $row = null;
        $isSelected = in_array($category->getId(), $this->getCategories());
        $isFolded   = $level > 1 && $this->getCategory()->getParentId() != $category->getParentId();
        $value = $this->_calculateCategoryValue($category->getId());

        if ($this->_getProductCount($category) || is_null($this->_getProductCount($category))) {

        	$finalLabel = Mage::helper('core')->htmlEscape($category->getName());
        	if ($filterLabel = $category->getFilterLabel()) {
        		$finalLabel = Mage::helper('core')->htmlEscape($filterLabel);
        	}

            $row = array(
                'label'       => $finalLabel,
                'url'         => $this->getCategoryUrl($value),
                'count'       => $this->_getProductCount($category),
                'level'       => $level,
                'id'          => $category->getId(),
                'value'       => $value,
                'parent_id'   => $category->getParentId(),
                'is_folded'   => $isFolded,
                'is_selected' => $isSelected,
                'apply_data'  => $this->getCategoryApplyData($category->getId()),
            );
        }
        return $row;
    }

}