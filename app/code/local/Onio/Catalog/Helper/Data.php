<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Catalog
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Catalog_Helper_Data extends Mage_Core_Helper_Abstract
{

    protected $_flagType = 9; // Attribute Set Id

    public function getProductNotation($_product)
    {
    	$reviewDatas = array(
    		'count' => 0,
    		'list' => array(),
    		'average' => 0
    	);
    	if (!$_product->getRatingSummary()) {
            Mage::getModel('review/review')
               ->getEntitySummary($_product, Mage::app()->getStore()->getId());
        }

    	if ($count = $_product->getRatingSummary()->getReviewsCount()) {
    		$reviewDatas['count'] = $count;
    	}

    	if ($summary = $_product->getRatingSummary()->getRatingSummary()) {
    		$summary = (floatval($summary)/100) * 5;
    		$summary = round($summary * 2) / 2;
    		$reviewDatas['average'] = $summary;
    	}

    	return $reviewDatas;
    }

    public function isFlagType($id) {
        return $id == $this->_flagType;
    }

    public function isCustomProduct($_product) {
        $opts = Mage::getSingleton('catalog/product_option')->getProductOptionCollection($_product);
        $pOptions = $opts->toArray();
        return (bool)Mage::helper('oniobase')->searchInArray($pOptions, 'Config_DRAP');
    }
}