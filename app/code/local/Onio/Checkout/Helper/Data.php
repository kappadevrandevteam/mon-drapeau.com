<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Checkout
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Checkout_Helper_Data extends Mage_Core_Helper_Abstract
{

	public function getHomeUrl() {
        return array(
            "label" => $this->__('Accueil'),
            "title" => $this->__('Accueil'),
            "link" => Mage::getUrl('')
        );
    }

    public function getCartUrl() {
        return array(
            "label" => $this->__('Mon panier'),
            "title" => $this->__('Mon panier'),
            "link" => Mage::getUrl('checkout/cart')
        );
    }

    public function getCartParams() {
        return array(
            "label" => $this->__('Mon panier'),
            "title" => $this->__('Mon panier')
        );
    }

    public function getCheckoutParams() {
        return array(
            "label" => $this->__('Commande'),
            "title" => $this->__('Commande')
        );
    }

}