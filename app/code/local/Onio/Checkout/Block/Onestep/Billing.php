<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Checkout
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Checkout_Block_Onestep_Billing extends AW_Onestepcheckout_Block_Onestep_Form_Address_Billing
{

	public function getAddressesList()
    {
    	$type = 'billing';
        if ($this->isCustomerLoggedIn()) {
            $options = array();
            foreach ($this->getCustomer()->getAddresses() as $address) {
                $options[] = array(
                    'value' => $address->getId(),
                    'label' => $address->format('oneline')
                );
            }
            $addressId = 0;
            $addressDetails = Mage::getSingleton('checkout/session')->getData('aw_onestepcheckout_form_values');
            if (isset($addressDetails[$type.'_address_id'])) {
                if (empty($addressDetails[$type.'_address_id'])) {
                    $addressId = 0;
                } else {
                    $addressId = $addressDetails[$type.'_address_id'];
                }
            } else {
                $addressId = $this->getQuote()->getBillingAddress()->getCustomerAddressId();
            }
            if (empty($addressId) && $addressId !== 0) {
                $address = $this->getCustomer()->getPrimaryBillingAddress();
                if ($address) {
                    $addressId = $address->getId();
                }
            }

            return array(
            	'selected' => $addressId,
            	'options' => $options
            );
        }
        return false;
    }
    
}