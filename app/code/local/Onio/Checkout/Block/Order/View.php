<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Checkout
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Checkout_Block_Order_View extends Mage_Sales_Block_Order_View
{

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('sales/order/view_light.phtml');
    }

    /**
     * Retrieve current order model instance
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        $session = Mage::getSingleton('checkout/type_onepage')->getCheckout();
        $lastOrderId = $session->getLastOrderId();
        $order = Mage::getModel('sales/order')->load($lastOrderId);
        return $order;
    }

}