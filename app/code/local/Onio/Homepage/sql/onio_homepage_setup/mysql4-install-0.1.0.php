<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    HOMEPAGE
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

$installer = $this;

$installer->startSetup();

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('onio_homepage')};
CREATE TABLE {$this->getTable('onio_homepage')} (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `image1` varchar(255) NOT NULL DEFAULT '',
  `image2` varchar(255) NOT NULL DEFAULT '',
  `image3` varchar(255) NOT NULL DEFAULT '',
  `label1` varchar(255) NOT NULL DEFAULT '',
  `label2` varchar(255) NOT NULL DEFAULT '',
  `label3` varchar(255) NOT NULL DEFAULT '',
  `link1` varchar(255) NOT NULL DEFAULT '',
  `link2` varchar(255) NOT NULL DEFAULT '',
  `link3` varchar(255) NOT NULL DEFAULT '',
  `button1` int(1) NOT NULL DEFAULT '0',
  `button2` int(1) NOT NULL DEFAULT '0',
  `button3` int(1) NOT NULL DEFAULT '0',
  `button_label1` varchar(255) NOT NULL DEFAULT '',
  `button_label2` varchar(255) NOT NULL DEFAULT '',
  `button_label3` varchar(255) NOT NULL DEFAULT '',
  `button_link1` varchar(255) NOT NULL DEFAULT '',
  `button_link2` varchar(255) NOT NULL DEFAULT '',
  `button_link3` varchar(255) NOT NULL DEFAULT '',
  `promo1` int(1) NOT NULL DEFAULT '0',
  `promo2` int(1) NOT NULL DEFAULT '0',
  `promo3` int(1) NOT NULL DEFAULT '0',

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup(); 