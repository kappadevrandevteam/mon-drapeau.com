<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Homepage
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Homepage_Adminhtml_HomepageController extends Mage_Adminhtml_Controller_action
{

	protected function _isAllowed()
	{
    	return Mage::getSingleton('admin/session')->isAllowed('cms/onio_homepage');
	}

	/**
     * Initialisation
     *
     */
	protected function _initAction()
	{
		$this->loadLayout()
			->_setActiveMenu('cms')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		return $this;
	}
	/**
     * Render index
     *
     */
	public function indexAction()
	{
		$this->_initAction()->renderLayout();
	}

	public function editAction()
	{
		$id = $this->getRequest()->getParam('id');
		$model = Mage::getModel('onio_homepage/homepage')->load($id);
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			Mage::register('homepage_data', $model);
			$this->loadLayout();
			$this->_setActiveMenu('cms/items');
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->_addContent($this->getLayout()->createBlock('onio_homepage/adminhtml_homepage_edit'))->_addLeft($this->getLayout()->createBlock('onio_homepage/adminhtml_homepage_edit_tabs'));
			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('onio_homepage')->__('Cette colonne est introuvable'));
			$this->_redirect('*/*/');
		}
	}

	/**
     * Save bloc
     *
     */
	public function saveAction()
	{
		if ($data = $this->getRequest()->getPost()) {
			$helper = Mage::helper('onio_homepage');
			$model = Mage::getModel('onio_homepage/homepage');

			$model->setData($data)->setId($this->getRequest()->getParam('id'));

			try {
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('onio_homepage')->__('La colonne a été enregistrée avec succès!'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('onio_homepage')->__('Enregistrement impossible'));
		$this->_redirect('*/*/');
	}

}
