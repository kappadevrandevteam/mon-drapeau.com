<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Homepage
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

require_once 'Mage/Adminhtml/controllers/System/ConfigController.php';
class Onio_Homepage_Adminhtml_System_ConfigController extends Mage_Adminhtml_System_ConfigController
{

	/**
     * Save configuration
     *
     */
    public function saveAction()
    {
        $session = Mage::getSingleton('adminhtml/session');
        /* @var $session Mage_Adminhtml_Model_Session */

        $groups = $this->getRequest()->getPost('groups');

        if (isset($_FILES['groups']['name']) && is_array($_FILES['groups']['name'])) {

        	$uplPath = Mage::helper('onio_homepage')->getFileDirPath() . DS ;

            /**
             * Carefully merge $_FILES and $_POST information
             * None of '+=' or 'array_merge_recursive' can do this correct
             */
            foreach($_FILES['groups']['name'] as $groupName => $group) {
                if (is_array($group)) {
                    foreach ($group['fields'] as $fieldName => $field) {
                        if (!empty($field['value'])) {
                        	$custom = false;
                        	if (is_array($field['value'])) {
                        		foreach ($field['value'] as $kgr => $vgr) {
                        			if (is_array($vgr)) {
                        				if (isset($vgr['flagimage'])) {
                        					$custom = true;
                        				}
                        			}
                        		}
                        	}
                        	if ($custom == false) {
                            	$groups[$groupName]['fields'][$fieldName] = array('value' => $field['value']);
                        	}
                        	else {
                        		foreach ($field['value'] as $kgr => $vgr) {
                        			if (isset($groups[$groupName]['fields'][$fieldName]['value'][$kgr])) {
                        				if ($_FILES['groups']['name']['products']['fields']['list']['value'][$kgr]["flagimage"] && $_FILES['groups']['name']['products']['fields']['list']['value'][$kgr]["flagimage"] != '') {
	                        				try {
												$uploader = new Varien_File_Uploader(
													array(
														'name' => $_FILES['groups']['name']['products']['fields']['list']['value'][$kgr]["flagimage"],
														'type' => $_FILES['groups']['type']['products']['fields']['list']['value'][$kgr]["flagimage"],
														'tmp_name' => $_FILES['groups']['tmp_name']['products']['fields']['list']['value'][$kgr]["flagimage"],
														'error' => $_FILES['groups']['error']['products']['fields']['list']['value'][$kgr]["flagimage"],
														'size' => $_FILES['groups']['size']['products']['fields']['list']['value'][$kgr]["flagimage"]
													)
												);
												$uploader->setAllowRenameFiles(true);
												$uploader->setFilesDispersion(false);
												$savedFile = $uploader->save($uplPath, $_FILES['groups']['name']['products']['fields']['list']['value'][$kgr]["flagimage"]);
												// Name
												$groups[$groupName]['fields'][$fieldName]['value'][$kgr]['image'] = Mage::helper('onio_homepage')->getMediaUrl('accueil'. DS .$savedFile['file']);
											} catch(Exception $e) {
												Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
											}
										}
                        			}
                        		}
                        	}
                        }
                    }
                }
            }
        }
        try {
            if (!$this->_isSectionAllowed($this->getRequest()->getParam('section'))) {
                throw new Exception(Mage::helper('adminhtml')->__('This section is not allowed.'));
            }

            // custom save logic
            $this->_saveSection();
            $section = $this->getRequest()->getParam('section');
            $website = $this->getRequest()->getParam('website');
            $store   = $this->getRequest()->getParam('store');
            Mage::getSingleton('adminhtml/config_data')
                ->setSection($section)
                ->setWebsite($website)
                ->setStore($store)
                ->setGroups($groups)
                ->save();

            // reinit configuration
            Mage::getConfig()->reinit();
            Mage::dispatchEvent('admin_system_config_section_save_after', array(
                'website' => $website,
                'store'   => $store,
                'section' => $section
            ));
            Mage::app()->reinitStores();

            // website and store codes can be used in event implementation, so set them as well
            Mage::dispatchEvent("admin_system_config_changed_section_{$section}",
                array('website' => $website, 'store' => $store)
            );
            $session->addSuccess(Mage::helper('adminhtml')->__('The configuration has been saved.'));
        }
        catch (Mage_Core_Exception $e) {
            foreach(explode("\n", $e->getMessage()) as $message) {
                $session->addError($message);
            }
        }
        catch (Exception $e) {
            $session->addException($e,
                Mage::helper('adminhtml')->__('An error occurred while saving this configuration:') . ' '
                . $e->getMessage());
        }

        $this->_saveState($this->getRequest()->getPost('config_state'));

        $this->_redirect('*/*/edit', array('_current' => array('section', 'website', 'store')));
    }

}
