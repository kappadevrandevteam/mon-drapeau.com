<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Homepage
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Homepage_Model_System_Config_Promos
{

    protected $_options = null;

	public function toOptionArray()
    {
        if ($this->_options === null) {
            $_categories = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToFilter('level', array('gt' => 1))
                ->addAttributeToSelect('name');

            $_categories->getSelect()
                ->order(array('path asc', 'position asc'));

            $this->_options = array();

            foreach ($_categories as $_category) {
                $level = $_category->getLevel();
                $level = $level-1;
                $sep = '';
                $sep = str_repeat("-", $level);
                $sep = str_repeat(" ", $level).$sep;

                $this->_options[] = array('value' => $_category->getEntityId(), 'label'=> $sep.' '.$_category->getName());
            }
        }

        return $this->_options;
    }
    
 
}