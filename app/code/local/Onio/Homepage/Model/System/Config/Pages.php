<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Homepage
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Homepage_Model_System_Config_Pages
{

    protected $_options = null;

	public function toOptionArray()
    {
        if ($this->_options === null) {
            $_pages = Mage::getModel('cms/page')
                ->getCollection();

            $this->_options = array();

            foreach ($_pages as $_page) {
                $this->_options[] = array('value' => $_page->getPageId(), 'label'=> $_page->getTitle());
            }
        }

        return $this->_options;
    }
    
 
}