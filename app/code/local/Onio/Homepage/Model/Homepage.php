<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Homepage
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Homepage_Model_Homepage extends Mage_Core_Model_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('onio_homepage/homepage');
	}	
}
