<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Homepage
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

/**
 * Adminhtml system config array field renderer
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Onio_Homepage_Block_Adminhtml_System_Config_Products extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{

    protected $_productRenderer;
    protected $_categoryRenderer;
    protected $_imageRenderer;
    protected $_promoRenderer;


    public function _prepareToRender()
    {
        // $this->addColumn('product', array(
        //     'label' => Mage::helper('onio_homepage')->__('Produit'),
        //     'class' => 'select2',
        //     'renderer' => $this->_getProductRenderer(),
        // ));
        // $this->addColumn('category', array(
        //     'label' => Mage::helper('onio_homepage')->__('Catégorie'),
        //     'renderer' => $this->_getCategoryRenderer(),
        // ));
        $this->addColumn('name', array(
            'label' => Mage::helper('onio_homepage')->__('Label'),
            'style' => 'width:150px;',
            'class' => 'input-text required-entry'
        ));
        $this->addColumn('url', array(
            'label' => Mage::helper('onio_homepage')->__('URL'),
            'style' => 'width:150px;',
            'class' => 'input-text required-entry'
        ));
        $this->addColumn('label', array(
            'label' => Mage::helper('onio_homepage')->__('Bouton'),
            'style' => 'width:150px;',
            'class' => 'input-text required-entry'
        ));
        $this->addColumn('image', array(
            'label' => Mage::helper('onio_homepage')->__('Image'),
            'class' => 'm-file',
            //'renderer' => $this->_getImageRenderer(),
        ));
        $this->addColumn('promo', array(
            'label' => Mage::helper('onio_homepage')->__('Promo'),
            'class' => 'm-checkbox',
            //'renderer' => $this->_getPromoRenderer(),
        ));
 
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('onio_homepage')->__('Ajouter un produit');
        $this->setTemplate('onio/homepage/array.phtml');
    }

    protected function  _getProductRenderer() 
    {
        if (!$this->_productRenderer) {
            $this->_productRenderer = $this->getLayout()->createBlock(
                'onio_homepage/adminhtml_system_config_products_render', '',
                array('is_render_to_js_template' => true)
            );
        }
        return $this->_productRenderer;
    }

    protected function  _getCategoryRenderer() 
    {
        if (!$this->_categoryRenderer) {
            $this->_categoryRenderer = $this->getLayout()->createBlock(
                'onio_homepage/adminhtml_system_config_category_render', '',
                array('is_render_to_js_template' => true)
            );
        }
        return $this->_categoryRenderer;
    }

    protected function  _getImageRenderer() 
    {
        if (!$this->_imageRenderer) {
            $this->_imageRenderer = $this->getLayout()->createBlock(
                'onio_homepage/adminhtml_system_config_image_render', '',
                array('is_render_to_js_template' => true)
            );
        }
        return $this->_imageRenderer;
    }

    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->_getProductRenderer()
                ->calcOptionHash($row->getData('product')),
            'selected="selected"'
        );
        $row->setData(
            'option_extra_attr_' . $this->_getCategoryRenderer()
                ->calcOptionHash($row->getData('category')),
            'selected="selected"'
        );

        // $row->setData(
        //     'option_extra_attr_' . $this->_getImageRenderer()
        //         ->calcOptionHash($row->getData('image')),
        //     'value="'.$row->getData('image').'"'
        // );

        $row->setData(
            'option_extra_attr_' . $this->_calcOptionHash($row->getData('promo')),
            'checked="checked"'
        );

    }

    public function _calcOptionHash($optionValue)
    {
        return sprintf('%u', crc32($this->getName() . $this->getId() . $optionValue));
    }

    /**
     * Render array cell for prototypeJS template
     *
     * @param string $columnName
     * @return string
     */
    protected function _renderCellTemplate($columnName)
    {
        if (empty($this->_columns[$columnName])) {
            throw new Exception('Wrong column name specified.');
        }
        
        $column     = $this->_columns[$columnName];
        $inputImgName  = $this->getElement()->getName() . '[#{_id}][flagimage]';
        $inputName  = $this->getElement()->getName() . '[#{_id}][' . $columnName . ']';

        if ($column['renderer']) {
            return $column['renderer']->setInputName($inputName)->setColumnName($columnName)->setColumn($column)
                ->toHtml();
        }
        if ($column['class'] == 'm-checkbox') {
            
            $selectedHtml = ' #{option_extra_attr_' . $this->_calcOptionHash(1) . '}';

            return '<input '.$selectedHtml.' type="checkbox" name="' . $inputName . '" value="1" ' .
                ($column['size'] ? 'size="' . $column['size'] . '"' : '') .
                (isset($column['style']) ? ' style="'.$column['style'] . '"' : '') . '/>';
        }
        else {
            if ($column['class'] == 'm-file') {
                $html = '<input type="hidden" name="' . $inputName . '" value="#{' . $columnName . '}" ' . ' class="required-entrys"/>';
                $html .= '<div class="upload-btn-wrapper">';
                $html .= '<img class="hp-preview" src="#{' . $columnName . '}" />';
                $html .= '<button class="btn">'.$this->__('Parcourir...').'</button>';
                $html .= '<input type="file" name="' . $inputImgName . '" value="#{' . $columnName . '}" ' .
                    ($column['size'] ? 'size="' . $column['size'] . '"' : '') . ' class="' .
                    (isset($column['class']) ? $column['class'].' hp-file' : 'input-text') . '"'.
                    (isset($column['style']) ? ' style="'.$column['style'] . '"' : '') . '/>';
                $html .= '</div>';
                return $html;
            }
            else {
                return '<input type="text" name="' . $inputName . '" value="#{' . $columnName . '}" ' .
                    ($column['size'] ? 'size="' . $column['size'] . '"' : '') . ' class="' .
                    (isset($column['class']) ? $column['class'] : 'input-text') . '"'.
                    (isset($column['style']) ? ' style="'.$column['style'] . '"' : '') . '/>';
            }
        }
    }


}