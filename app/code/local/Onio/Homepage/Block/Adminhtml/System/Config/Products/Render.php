<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Homepage
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Homepage_Block_Adminhtml_System_Config_Products_Render extends Mage_Core_Block_Html_Select
{

    protected $_productList = null;

    public function _toHtml()
    {
        if (!$this->_productList) {
        
            $collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('name');
            $collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
            $collection->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);

            $this->_productList = $collection;
        }

        foreach ($collection as $_product) {            
            $this->addOption($_product->getId(), $_product->getName());
        }
        
        $this->setClass('select_admin_homepage required_entry');

        return parent::_toHtml();
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }
}
