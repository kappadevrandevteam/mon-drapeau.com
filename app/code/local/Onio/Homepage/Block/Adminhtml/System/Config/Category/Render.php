<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Homepage
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Homepage_Block_Adminhtml_System_Config_Category_Render extends Mage_Core_Block_Html_Select
{

    protected $_categoryList = null;

    public function _toHtml()
    {
        if (!$this->_categoryList) {

            $_categories = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToFilter('level', array('gt' => 1))
                ->addAttributeToSelect('name');

            $_categories->getSelect()
                ->order(array('path asc', 'position asc'));

            $this->_categoryList = $_categories;
        }

        foreach ($_categories as $_category) {
            $this->addOption($_category->getEntityId(), $_category->getName());
        }
        
        $this->setClass('select_admin_homepage required_entry');

        return parent::_toHtml();
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }
}