<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Homepage
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Homepage_Block_Adminhtml_Homepage_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'onio_homepage';
        $this->_controller = 'adminhtml_homepage';
        
        $this->_updateButton('save', 'label', Mage::helper('onio_homepage')->__('Enregistrer la colonne'));

        $this->_removeButton('delete');
        $this->_removeButton('reset');
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__("Sauvegarder et continuer l'édition"),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('response') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'response');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'response');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    public function getHeaderText()
    {
        if( Mage::registry('homepage_data') && Mage::registry('homepage_data')->getId() ) {
            return Mage::helper('onio_homepage')->__('Edition de la colonne');
        }
    }
}