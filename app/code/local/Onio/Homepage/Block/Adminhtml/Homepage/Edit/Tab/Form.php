<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Homepage
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Homepage_Block_Adminhtml_Homepage_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	
	protected function _prepareForm() {
		$form = new Varien_Data_Form();
		$this->setForm($form);

		$formDatas = new Varien_Object();

		if ( Mage::getSingleton('adminhtml/session')->getHomepageData() ) {
			$formDatas = Mage::getSingleton('adminhtml/session')->getHomepageData();
			Mage::getSingleton('adminhtml/session')->setHomepageData(null);
		}
		else if ( Mage::registry('homepage_data') ) {
			$formDatas = Mage::registry('homepage_data');
		}

		$curId = $formDatas->getId();
		$maxLines = 3;
		if ($curId == 1 || $curId == 2) {
			$maxLines = 2;
		}

		for ($i=1; $i<= $maxLines ; $i++) {

			$fieldset = $form->addFieldset('homepage_form'.$i, array('legend' => Mage::helper('onio_homepage')->__('Ligne produit %s', $i)));
			$fieldset->addType('mediachooser','AntoineK_MediaChooserField_Data_Form_Element_Mediachooser');

			$fieldset->addField('image'.$i, 'mediachooser', array(
				'label' => Mage::helper('onio_homepage')->__('Image'),
				'required' => true,
				'name' => 'image'.$i
			));

			$fieldset->addField('label'.$i, 'text', array(
				'label' => Mage::helper('onio_homepage')->__('Texte'),
				'required' => true,
				'name' => 'label'.$i
			));

			$fieldset->addField('link'.$i, 'text', array(
				'label' => Mage::helper('onio_homepage')->__('Lien du bloc'),
				'required' => true,
				'name' => 'link'.$i,
				'note' => '<p class="note"><span>Ex : https://www.mon-drapeau.com/lien</span></p>',
			));

			$fieldset->addField('promo'.$i, 'select', array(
	            'label'     => Mage::helper('onio_homepage')->__('Flag Promo'),
	            'title'     => Mage::helper('onio_homepage')->__('Flag Promo'),
	            'name'      => 'promo'.$i,
	            'options'   => array(
	                '1' => Mage::helper('onio_homepage')->__('Oui'),
	                '0' => Mage::helper('onio_homepage')->__('Non'),
	            ),
	        ));

			$fieldset->addField('button'.$i, 'select', array(
	            'label'     => Mage::helper('onio_homepage')->__('Ajouter un bouton'),
	            'title'     => Mage::helper('onio_homepage')->__('Ajouter un bouton'),
	            'name'      => 'button'.$i,
	            'options'   => array(
	                '1' => Mage::helper('onio_homepage')->__('Oui'),
	                '0' => Mage::helper('onio_homepage')->__('Non'),
	            ),
	        ));

			$fieldset->addField('button_label'.$i, 'text', array(
				'label' => Mage::helper('onio_homepage')->__('Label du bouton'),
				'required' => true,
				'name' => 'button_label'.$i
			));

			$fieldset->addField('button_link'.$i, 'text', array(
				'label' => Mage::helper('onio_homepage')->__('Lien du bouton'),
				'required' => true,
				'name' => 'button_link'.$i,
				'note' => '<p class="note"><span>Ex : https://www.mon-drapeau.com/lien</span></p>',
			));
		}


		$depends = $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence');

		for ($j=1; $j<= $maxLines ; $j++) {

			$depends->addFieldMap('button'.$j, 'button'.$j)
				->addFieldMap('button_label'.$j, 'button_label'.$j)
				->addFieldMap('button_link'.$j, 'button_link'.$j);

			$depends->addFieldDependence(
				'button_label'.$j,
				'button'.$j,
				'1'
			)->addFieldDependence(
				'button_link'.$j,
				'button'.$j,
				'1'
			);
		}

		$this->setChild('form_after', $depends);

		$form->setValues($formDatas);
		
		return parent::_prepareForm();
	}

}
