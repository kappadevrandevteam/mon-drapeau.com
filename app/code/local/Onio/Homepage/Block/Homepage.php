<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Homepage
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Homepage_Block_Homepage extends Mage_Core_Block_Template
{

    protected $_helper;
    protected $_cmsHelper;
    protected $_homepageConfig;

    /**
     * Constructor. Set var, get config
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_helper = Mage::helper('onio_homepage'); // Define helper
        $this->_cmsHelper = Mage::helper('cms/page');
        $this->setHomeConfig();
    }

    /**
     * Get full path of media URL
     * @return string
     */
    public function getMediaUrl($srcImage)
    {
        $mediaUrl = Mage::getBaseUrl('media') . $srcImage;
        return $mediaUrl;
    }

    public function getFileDirUrl($file = '')
    {
        return Mage::getBaseUrl('media') . 'slider' . DS . $file;
    }

    /**
     * Retrieve From Config
     * @return array
     */
    public function setHomeConfig()
    {
        if (!$this->_homepageConfig) {
            $this->_homepageConfig = Mage::helper('onio_homepage')->getHomeConfig();
        }

        return $this->_homepageConfig;
    }

    public function getCustomImage()
    {
        return isset($this->_homepageConfig['custom']['image']) ? $this->getMediaUrl($this->_homepageConfig['custom']['image']) : '';
    }

    public function getPromoLink()
    {
        if ($this->_homepageConfig['custom']['add_promo'] == 1) {
            $categoryLink = Mage::getModel("catalog/category")->load($this->_homepageConfig['custom']['promo_page'])->getUrl();
            return array(
                'label' => $this->_homepageConfig['custom']['promo_label'],
                'link' => $categoryLink
            );
        }
        return false;
    }

    public function getProductList() {
        $productsConfig = isset($this->_homepageConfig['products']['list']) ? $this->_homepageConfig['products']['list'] : false;
        $list = array();

        $categories = array();
        $products = array();

        if ($productsConfig) {
            $productsConfig = Mage::helper('oniobase')->maybeUnserialize($productsConfig);
            // foreach ($productsConfig as $_configLine) {
            //     $products[$_configLine['product']] = $_configLine['product'];
            //     $categories[$_configLine['category']] = $_configLine['category'];
            // }

            // if ($products) {
            //     $_productCollection = Mage::getModel('catalog/product')
            //         ->getCollection()
            //         ->addAttributeToSelect(array('name', 'url_path', 'small_image'))
            //         ->addAttributeToFilter('id', array('in' => $products))
            //         ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            //         ->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
            //     if ($_productCollection) {
            //         foreach ($_productCollection as $_product) {

            //             $img = Mage::helper('catalog/image')->init($_product, 'small_image')->resize(350);
                        
            //             $products[$_product->getId()] = array(
            //                 'name' => $_product->getName(),
            //                 'url' => $_product->getUrlPath(),
            //                 'image' => $img
            //             );
            //         }
            //     }
            // }

            // if ($categories) {
            //     $_categoriesCollection = Mage::getModel('catalog/category')
            //         ->getCollection()
            //         ->addAttributeToSelect('entity_id')
            //         ->addAttributeToFilter('entity_id', array('in' => $categories))
            //         ->addAttributeToFilter('is_active', 1);
            //     if ($_categoriesCollection) {
            //         foreach ($_categoriesCollection as $_category) {
            //             $categories[$_category->getEntityId()] = $_category->getUrl($_category);
            //         }
            //     }
            // }

            foreach ($productsConfig as $_configLine) {

                // if (isset($products[$_configLine['product']]) && isset($categories[$_configLine['category']])) {
                //     $list[] = array(
                //         'promo' => isset($_configLine['promo']) ? true : false,
                //         'product' => $products[$_configLine['product']],
                //         'category_link' => $categories[$_configLine['category']],
                //         'button' => $_configLine['label']
                //     );
                // }
                $list[] = array(
                    'promo' => isset($_configLine['promo']) ? true : false,
                    'image' => $_configLine['image'],
                    'name' => $_configLine['name'],
                    'url' => $_configLine['url'],
                    'button' => $_configLine['label']
                );
            }


        }

        return $list;
    }


    public function getIcons()
    {
        $iconsConfig = $this->_homepageConfig['pictos'];
        $icons = array();
        $lines = 4;
        for ($i=1; $i<= $lines ; $i++) { 
            $iconPath = $this->getMediaUrl($iconsConfig['p'.$i.'img']);
            //$iconPath = @file_get_contents($iconPath);
            $icons[] = array(
                'icon' => $iconPath,
                'label' => $iconsConfig['p'.$i.'label'],
                'text' => $iconsConfig['p'.$i.'text'],
            );
        }
        return $icons;
    }

    public function getAbout()
    {
        $aboutConfig = $this->_homepageConfig['about'];
        return $aboutConfig;
    }

    public function getRelay()
    {
        $relayConfig = $this->_homepageConfig['relay'];
        $relay = array();
        $lines = 3;
        for ($i=1; $i<= $lines ; $i++) { 
            $iconPath = $this->getMediaUrl($relayConfig['p'.$i.'img']);
            //$iconPath = @file_get_contents($iconPath);
            $relay[] = array(
                'icon' => $iconPath,
                'label' => $relayConfig['p'.$i.'label'],
                'text' => $relayConfig['p'.$i.'text'],
                'link' => isset($relayConfig['p'.$i.'link']) ? $relayConfig['p'.$i.'link'] : false,
                'cms' => isset($relayConfig['p'.$i.'cms']) ? $this->_cmsHelper->getPageUrl($relayConfig['p'.$i.'cms']) : false,
                'labelcms' => isset($relayConfig['p'.$i.'labelcms']) ? $relayConfig['p'.$i.'labelcms'] : false,
            );
        }
        return $relay;
    }

    public function getGrid()
    {
        $gridConfig = $this->_homepageConfig['grid'];
        $grid = array();
        $lines = 2;
        for ($i=1; $i<= $lines ; $i++) { 
            $iconPath = $this->getMediaUrl($gridConfig['p'.$i.'img']);
            //$iconPath = @file_get_contents($iconPath);
            $grid[] = array(
                'icon' => $iconPath,
                'label' => $gridConfig['p'.$i.'label'],
                'title' => $gridConfig['p'.$i.'title'],
                'text' => $gridConfig['p'.$i.'text'],
                'link' => isset($gridConfig['p'.$i.'link']) ? $gridConfig['p'.$i.'link'] : false,
                'cms' => isset($gridConfig['p'.$i.'cms']) ? $this->_cmsHelper->getPageUrl($gridConfig['p'.$i.'cms']) : false,
                'labelcms' => isset($gridConfig['p'.$i.'labelcms']) ? $gridConfig['p'.$i.'labelcms'] : false,
            );
        }
        return $grid;
    }

    public function getBlocs()
    {
        $blocsConfig = $this->_homepageConfig['blocs'];
        return $blocsConfig;
    }

    public function getContact()
    {
        $contactConfig = $this->_homepageConfig['contact'];
        return $contactConfig;
    }

    public function getHomeList() {
        $hp = array();
        $list = Mage::getModel('onio_homepage/homepage')->getCollection();
        return $list ?: false;
        if ($list) {
            foreach ($list as $_item) {
                $hp[$_item->getId()] = $_item;
            }
        }
        return $hp;
    }

}
