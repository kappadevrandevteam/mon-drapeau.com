<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Theme
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio.
 */

class Onio_Theme_Helper_Data extends Mage_Catalog_Helper_Data
{
    
    protected $_headerText;
    protected $_footerText;

    /**
     * Return Header Text From Config
     *
     * @return String
     */
    public function getHeaderText()
    {
        if (!$this->_headerText) {
            $this->_headerText = Mage::getStoreConfig('header/general/header_text');
        }

        return $this->_headerText;
    }

    /**
     * Return Footer Text From Config
     *
     * @return String
     */
    public function getFooterText()
    {
        if (!$this->_footerText) {
            $this->_footerText = Mage::getStoreConfig('footer/general/seo_text');
        }

        return $this->_footerText;
    }
}