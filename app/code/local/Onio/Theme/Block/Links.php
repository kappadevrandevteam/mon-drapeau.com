<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Theme
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio.
 */

class Onio_Theme_Block_Links extends Mage_Core_Block_Template
{

    /**
     * Add Links To Header
     *
     * @return Onio_Cms_Block_Links
     */
    public function addHeaderLinks()
    {
        $parentBlock = $this->getParentBlock();
        if ($parentBlock) {
            $_helper = Mage::helper('onio_theme');
            $storeText = $_helper->__('Nos magasins');

            $storeUrl = $_helper->getStoreLink();

            $parentBlock->addLink($storeText, 'javascript:void(0)', $storeText, false, array(), 50, null, 'class="top-link-store trigger-store visible-sm visible-xs"');
            $parentBlock->addLink($storeText, $storeUrl, $storeText, false, array(), 50, null, 'class="top-link-store hidden-sm hidden-xs"');

            $searchText = $_helper->__('Rechercher un produit');
            $searchUrl = 'javascript:void(0)';

            $parentBlock->addLink($searchText, $searchUrl, $searchText, false, array(), 50, null, 'id="search_button" class="top-link-search"');

            $customerText = $_helper->__('Mon compte');
            $customerUrl = Mage::helper('customer')->getDashboardUrl();


            $parentBlock->addLinkBlock('account_block');

            //$parentBlock->addLink($customerText, $customerUrl, $customerText, false, array(), 50, null, 'id="customer_button" class="top-link-customer"');

            $checkoutText = $_helper->__('Mon panier');
            $checkoutUrl = 'javascript:void(0)';

            $parentBlock->addLink($checkoutText, $checkoutUrl, $checkoutText, false, array(), 50, null, 'id="checkout_button" class="top-link-checkout"');

        }
        return $this;
    }

}