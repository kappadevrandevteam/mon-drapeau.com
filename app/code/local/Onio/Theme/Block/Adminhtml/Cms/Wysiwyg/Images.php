<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Theme
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio.
 */

class Onio_Theme_Block_Adminhtml_Cms_Wysiwyg_Images extends Mage_Adminhtml_Block_Cms_Wysiwyg_Images_Content_Files
{
	/**
     * File thumb URL getter
     *
     * @param  Varien_Object $file
     * @return string
     */
    public function getFileThumbUrl(Varien_Object $file)
    {
    	$mimeType = mime_content_type($file->getFilename());
        if ($mimeType == 'image/svg+xml') {
        	return $file->getUrl();
        }
        return $file->getThumbUrl();
    }
}
