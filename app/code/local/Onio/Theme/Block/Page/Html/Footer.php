<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Theme
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio.
 */

class Onio_Theme_Block_Page_Html_Footer extends Mage_Core_Block_Template
{

    /**
     * getCookieBannerMessage 
     * Function to get admin msg cookie banner
     * 
     * @return string
     */
    public function getCookieBannerMessage() 
    {
        $cookieMessage = Mage::getStoreConfig('footer/cookie/message');
        return $cookieMessage;
    }

    /**
     * hasCookieBanner 
     * function to get the cookie of cookie banner (to display or not)
     * 
     * @return bool
     */
    public function hasCookieBanner()
    {
        return isset($_COOKIE['monDrapeauCookie']) ? $_COOKIE['monDrapeauCookie'] : false;
    }

    public function getLogoSrc()
    {
        if (empty($this->_data['footer_logo_src'])) {
            $this->_data['footer_logo_src'] = Mage::getStoreConfig('footer/general/footer_logo_src');
        }
        return $this->getSkinUrl($this->_data['footer_logo_src']);
    }
}