<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Theme
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio.
 */

class Onio_Theme_Model_Cms_Wysiwyg_Images_Storage extends Mage_Cms_Model_Wysiwyg_Images_Storage
{
    
     /**
     * Create thumbnail for image and save it to thumbnails directory
     *
     * AAI CHANGES: don't bother resizing SVG files
     *
     * @param string $source Image path to be resized
     * @param bool $keepRation Keep aspect ratio or not
     * @return bool|string Resized filepath or false if errors were occurred
     */
    public function resizeFile($source, $keepRation = true)
    {
        $mimeType = mime_content_type($source);
        if ($mimeType == 'image/svg+xml' || $mimeType == 'video/mp4') {
            return false;
        } else {
            return parent::resizeFile($source, $keepRation);
        }
    }
    
}