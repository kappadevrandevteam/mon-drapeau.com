<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Theme
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio.
 */

class Onio_Theme_Model_Core_File_Validator_Image extends Mage_Core_Model_File_Validator_Image
{
    
    /**
     * Validation callback for checking is file is image
     *
     * @param  string $filePath Path to temporary uploaded file
     * @return null
     * @throws Mage_Core_Exception
     */
    public function validate($filePath)
    {
        if (mime_content_type($filePath) == 'image/svg+xml' || mime_content_type($filePath) == 'video/mp4') {
            return null;
        } else {
            return parent::validate($filePath);
        }
    }
    
}