<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Guides
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('onio_custom_pattern')};
CREATE TABLE {$this->getTable('onio_custom_pattern')} (
  `option_id` int(11) unsigned NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`option_id`),
  KEY `IDX_MGTO_ONIO_CUSTOM_OPTION_ID` (`option_id`),
  CONSTRAINT `FK_MGTO_ONIO_CUSTOMOPTIONID_OPTIONID` FOREIGN KEY (`option_id`) REFERENCES `{$this->getTable('eav_attribute_option_value')}` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('onio_custom_cliparts')};
CREATE TABLE {$this->getTable('onio_custom_cliparts')} (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `set` TEXT NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup(); 