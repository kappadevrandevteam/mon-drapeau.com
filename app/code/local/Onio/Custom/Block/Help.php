<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Block_Help extends Mage_Core_Block_Template
{

	public function getHelp()
	{
		$html = '';

		$page = Mage::getModel('cms/page');
		$page->setStoreId(Mage::app()->getStore()->getId());
		$page->load('guide-outil-de-perso','identifier'); //EDIT IN THIS LINE
		$helper = Mage::helper('cms');
		$processor = $helper->getPageTemplateProcessor();
		$html .= $processor->filter($page->getContent());

		return $html;
	}
}
