<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Block_Adminhtml_Pattern_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('patternGrid');
		$this->setDefaultSort('option_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('onio_custom/pattern')->getCollection();
		$attributeOptionTable = Mage::getSingleton('core/resource')->getTableName('eav/attribute_option_value');
		$collection
			->getSelect()
		    ->join(
		    	array('ao' => $attributeOptionTable),
		    	'main_table.option_id = ao.option_id',
		    	array('ao.value')
		    );

		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('option_id', array(
			'header' => Mage::helper('onio_custom')->__('Finition'),
			'align' => 'right',
			'width' => '120px',
			'index' => 'value',
			
		));

		$this->addColumn('image', array(
			'header' => Mage::helper('onio_custom')->__('IMAGE'),
			'index' => 'image',
			'renderer'  => 'Onio_Custom_Block_Adminhtml_Pattern_Renderer',
		));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edit', array('option_id' => $row->getOptionId()));
	}

}
