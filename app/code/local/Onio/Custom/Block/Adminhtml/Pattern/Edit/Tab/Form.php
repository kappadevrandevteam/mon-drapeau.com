<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Block_Adminhtml_Pattern_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	
	protected function _prepareForm() {
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		$fieldset = $form->addFieldset('pattern_form', array('legend' => Mage::helper('onio_custom')->__('Général')));

		$fieldset->addField('value', 'text', array(
			'label' => Mage::helper('onio_custom')->__('Finition'),
			'title'  => Mage::helper('onio_custom')->__('Finition'),
			'required' => true,
			'readonly' => true,
			'disabled' => true,
			'name' => 'value',
		));

        $fieldset->addType('mediachooser','AntoineK_MediaChooserField_Data_Form_Element_Mediachooser');

		$fieldset->addField('image', 'textarea', array(
			'label' => Mage::helper('onio_custom')->__('Description'),
			'required' => true,
			'name' => 'image'
		));
		
		if (Mage::getSingleton('adminhtml/session')->getPatternData()) {
			$datas = Mage::getSingleton('adminhtml/session')->getPatternData();
			Mage::getSingleton('adminhtml/session')->setPatternData(null);
		} elseif (Mage::registry('pattern_data')) {
			$datas = Mage::registry('pattern_data');
		}

		$form->setValues($datas);
		
		return parent::_prepareForm();
	}

}
