<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Block_Adminhtml_Pattern_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('patern_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('onio_custom')->__('Informations'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('onio_custom')->__('Image'),
          'title'     => Mage::helper('onio_custom')->__('Image'),
          'content'   => $this->getLayout()->createBlock('onio_custom/adminhtml_pattern_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}