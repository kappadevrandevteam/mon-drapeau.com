<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Block_Adminhtml_Pattern_Renderer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
      public function render(Varien_Object $row)
      {
          $value =  $row->getData($this->getColumn()->getIndex());
          if ($value == '') {
          	return '<strong style="color:red;">'.Mage::helper('onio_custom')->__('Texte manquant').'</strong>';
          }
          else {
          	return '<strong style="color:#46b020;">'.Mage::helper('onio_custom')->__('Texte OK').'</strong>';
          }
      }
}