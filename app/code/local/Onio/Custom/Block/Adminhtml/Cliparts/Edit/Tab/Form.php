<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Block_Adminhtml_Cliparts_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	
	protected function _prepareForm() {
		$form = new Varien_Data_Form();
		$this->setForm($form);

		$formDatas = new Varien_Object();

		if ( Mage::getSingleton('adminhtml/session')->getClipartsData() ) {
			$formDatas = Mage::getSingleton('adminhtml/session')->getClipartsData();
			Mage::getSingleton('adminhtml/session')->setClipartsData(null);
		}
		else if ( Mage::registry('cliparts_data') ) {
			$formDatas = Mage::registry('cliparts_data');
		}
		
		$fieldset = $form->addFieldset('cliparts_form', array('legend' => Mage::helper('onio_custom')->__('Général')));

		$fieldset->addField('name', 'text', array(
			'label' => Mage::helper('onio_custom')->__('Nom'),
			'title'  => Mage::helper('onio_custom')->__('Nom'),
			'required' => true,
			'name' => 'name',
			'note' => Mage::helper('onio_custom')->__('Type de bibliothèque')
		));

		$fieldset->addField('set', 'textarea', array(
			'label' => Mage::helper('onio_custom')->__('Collection'),
			'required' => true,
			'name' => 'set',
			'value' => isset($formDatas['set']) ? $formDatas['set'] : ''
		));

		$form->getElement('set')->setRenderer(
			$this->getLayout()->createBlock('onio_custom/adminhtml_cliparts_edit_render')
		);

		$form->setValues($formDatas);
		
		return parent::_prepareForm();
	}

}
