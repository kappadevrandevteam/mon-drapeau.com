<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Block_Adminhtml_Pattern extends Mage_Adminhtml_Block_Widget_Grid_Container {

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_pattern';
        $this->_blockGroup = 'onio_custom';
        $this->_headerText = Mage::helper('onio_custom') -> __('Gestion des finitions');
        
        parent::__construct();
        $this->_removeButton('add');
    }

}
