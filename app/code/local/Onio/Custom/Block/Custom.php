<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Block_Custom extends Mage_Core_Block_Template
{

	public function getPattern()
	{
		$currentSku = Mage::app()->getRequest()->getParam('option_id');
		if ($currentSku) {
			$imgPath = $this->getSkinUrl('images/', array('_secure'=>true));
			if (preg_match('/DRA/', $currentSku)) {
				$pattern['sku'] = 'DRA';
				$pattern['base'] = $imgPath.'editor/drapeaux/base.png';
				$pattern['image'] = $imgPath.'editor/drapeaux/bg_drapeau.png';
				$pattern['base_params'] = '{"width": 960, "height": 540, "left": 608, "top": 291}';
				$pattern['image_params'] = '{"width": 995, "height": 569, "left": 605, "top": 295, "colors": "#FFFFFF", "colorLinkGroup": "Base"}';
				return $pattern;
			}
            elseif (preg_match('/GUI/', $currentSku)) {
            	$pattern['sku'] = 'GUI';
            	$pattern['base'] = $imgPath.'editor/drapeaux/base_guirlande.png';
            	$pattern['image'] = $imgPath.'editor/drapeaux/bg_guirlande.png';
            	$pattern['base_params'] = '{"width": 365, "height": 540, "left": 605, "top": 290}';
				$pattern['image_params'] = '{"width": 391, "height": 565, "left": 603, "top": 285, "colors": "#FFFFFF", "colorLinkGroup": "Base"}';
            	return $pattern;
            }
            elseif (preg_match('/BEA/', $currentSku)) {
            	$pattern['sku'] = 'BEA';
            	$pattern['base'] = $imgPath.'editor/drapeaux/base_beach.png';
            	$pattern['image'] = $imgPath.'editor/drapeaux/bg_beach.png';
            	$pattern['base_params'] = '{"width": 167, "height": 550, "left": 611, "top": 294}';
				$pattern['image_params'] = '{"width": 191, "height": 565, "left": 603, "top": 285, "colors": "#FFFFFF", "colorLinkGroup": "Base"}';
            	return $pattern;
            }
            return false;
		}
		return false;
	}

	public function getCliparts()
	{
		$cliparts = Mage::getModel('onio_custom/cliparts')->getCollection();
		return $cliparts;
	}

}
