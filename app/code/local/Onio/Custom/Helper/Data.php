<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Helper_Data extends Mage_Core_Helper_Abstract
{

	protected $_basePath;
	protected $_attributeId = 133; // Attribute Finition

	public function __construct()
	{
		$this->_basePath = Mage::getBaseDir('media') . DS . 'personnalisation' . DS . 'cliparts';
		$this->_userPath = Mage::getBaseDir('media') . DS . 'user_files';
		$this->_checkFolder($this->_basePath, $this->_userPath);
	}

	public function getUploadPath()
	{
		return $this->_userPath;
	}

	public function getUploadUrl()
	{
		return Mage::getBaseUrl('media') . 'user_files' . DS;
	}

	public function getFileDirUrl($file = '')
	{
		return Mage::getBaseUrl('media') . 'personnalisation' . DS . 'cliparts' . DS . $file;
	}

	public function getFileDirPath()
	{
		return Mage::getBaseDir('media') . DS . 'personnalisation' . DS . 'cliparts';
	}

	protected function _checkFolder($mediaDir, $userDir)
	{
		if (!file_exists($mediaDir)) {
			mkdir($mediaDir, 0755, true);
		}
		if (!file_exists($userDir)) {
			mkdir($userDir, 0755, true);
		}
	}

	public function getMediaUrl($srcImage)
	{
		$mediaUrl = Mage::getBaseUrl('media') . $srcImage;
		return $mediaUrl;
	}

	public function getAttributeId()
    {
        return $this->_attributeId;
    }

    public function getItemOption($_item)
    {
    	$productOptionBase = $_item->getProduct()->getTypeInstance(true)->getOrderOptions($_item->getProduct());
    	
    	if (isset($productOptionBase['info_buyRequest']['options'])) {
    		$productOption = $productOptionBase['info_buyRequest']['options'];
    		$productOption = is_array($productOption) ? array_pop($productOption) : $productOption;
    		try {
    			$productOption = Mage::helper('core')->jsonDecode($productOption);	
    		} catch (Exception $e) {
    			if (isset($productOptionBase['options'])) {
    				$basehelper = Mage::helper('oniobase');
    				$productOptionBase['options'] = array_pop($productOptionBase['options']);
    				// Region Case
    				if ($basehelper->searchInArray($productOptionBase['options'], 'Région')) {
    					
    					return array(
	    					'type' => 'DATAS',
	    					'label' => $productOptionBase['options']['label'],
	    					'value' => $productOptionBase['options']['value']
	    				);
    				}
    				// Country Case
    				if ($basehelper->searchInArray($productOptionBase['options'], 'Pays')) {
    					return array(
	    					'type' => 'DATAS',
	    					'label' => $productOptionBase['options']['label'],
	    					'value' => $productOptionBase['options']['value']
	    				);
    				}
    			}
    		}
    		if (is_array($productOption)) {
    			if (isset($productOption['type'])) {
    				if ($productOption['type'] == 'EDITOR') {
		    			if (isset($productOption['image'])) {
		    				$finalImage = $productOption['image'];
		    				$finalImage = str_replace('.png', '', $finalImage);
		    				return array(
		    					'type' => 'EDITOR',
		    					'url' => Mage::getUrl('custom/design/view', array('file' => $finalImage))
		    				);
		    			}
		    		}
		    		if ($productOption['type'] == 'UPLOAD') {
		    			return array(
	    					'type' => 'DATAS',
	    					'label' => '<strong>'.$this->__('Design personnalisé').'</strong>',
	    					'value' => false
	    				);
		    		}
	    		}
    		}
    	}
    	return false;
    }

    public function getOrderItemOption($_item)
    {
    	$productOptionBase = $_item->getProductOptions();
    	if (isset($productOptionBase['info_buyRequest']['options'])) {
    		$productOption = $productOptionBase['info_buyRequest']['options'];
    		$productOption = is_array($productOption) ? array_pop($productOption) : $productOption;
    		try {
    			$productOption = Mage::helper('core')->jsonDecode($productOption);	
    		} catch (Exception $e) {
    			if (isset($productOptionBase['options'])) {
    				$basehelper = Mage::helper('oniobase');
    				$productOptionBase['options'] = array_pop($productOptionBase['options']);
    				// Region Case
    				if ($basehelper->searchInArray($productOptionBase['options'], 'Région')) {
    					
    					return array(
	    					'type' => 'DATAS',
	    					'label' => $productOptionBase['options']['label'],
	    					'value' => $productOptionBase['options']['value']
	    				);
    				}
    				// Country Case
    				if ($basehelper->searchInArray($productOptionBase['options'], 'Pays')) {
    					return array(
	    					'type' => 'DATAS',
	    					'label' => $productOptionBase['options']['label'],
	    					'value' => $productOptionBase['options']['value']
	    				);
    				}
    			}
    		}
    		if (is_array($productOption)) {
    			if (isset($productOption['type'])) {
    				return $productOption;
	    		}
    		}
    	}
    	return false;
    }
    public function getDlFile($opt, $orderId) {
    	$filename = 'drap_perso_'.$orderId.'_genfile.zip';
    	if (file_exists($this->_userPath.DS.$filename)) {
    		return $this->getUploadUrl().$filename;
    	}

    	$zip = new ZipArchive();
    	if ($zip->open($this->_userPath.DS.$filename, ZipArchive::CREATE) === TRUE) {
    		
    		$uplUrl = $this->getUploadUrl();
    		$updClip = $this->getFileDirUrl();
    		

	    	if ($opt['type'] == 'EDITOR') {
	    		$image = $opt['image'];
    			$zip->addFile($this->_userPath.DS.$image, 'fichier_final.png');
    			if (isset($opt['designer'])) {
    				$designer = array_pop($opt['designer']);
    				$toDelete = array();
    				if (isset($designer['elements'])) {
    					$itext = 0;
    					foreach ($designer['elements'] as $_element) {
    						if (isset($_element['source'])) {
    							if (preg_match('/bg_drapeau/', $_element['source'])) {
    								if (isset($_element['parameters']['fill'])) {
    									$fill = $_element['parameters']['fill'];
    									if ($fill != '#ffffff' && $fill != '#FFFFFF') {
    										$addedBgFile = uniqid().'.txt';
	    									$fileBgContent = "Couleur de fond : $fill";
	    									file_put_contents($this->_userPath.DS.$addedBgFile, $fileBgContent);
	    									$zip->addFile($this->_userPath.DS.$addedBgFile, 'fond.txt');
	    									$toDelete[] = $this->_userPath.DS.$addedBgFile;
    									}
    								}
    							}
    							if (!preg_match('/base/', $_element['source']) && !preg_match('/bg_drapeau/', $_element['source'])) {
    								if (preg_match('/user_files/', $_element['source'])) {
	    								$finalFileElement = str_replace($uplUrl, $this->_userPath.DS, $_element['source']);
	    								$finalFileName = str_replace($uplUrl, '', $_element['source']);
	    								$zip->addFile($finalFileElement, $finalFileName);
	    							}
    								
    								else if (preg_match('/personnalisation/', $_element['source'])) {
	    								$finalFileElement = str_replace($updClip, $this->_basePath.DS, $_element['source']);
    									$finalFileName = str_replace($updClip, '', $_element['source']);
    									$zip->addFile($finalFileElement, $finalFileName);
	    							}
	    							else {
	    								$finalFileElement = $_element['source'];
	    								if (isset($_element['parameters']['fontFamily'])) {
	    									$font = $_element['parameters']['fontFamily'];
	    									$addedTextFile = uniqid().'.txt';
	    									$fileContent = "Texte : $finalFileElement\n";
	    									$fileContent .= "Police : $font";
	    									$itext ++;
	    									file_put_contents($this->_userPath.DS.$addedTextFile, $fileContent);
	    									$zip->addFile($this->_userPath.DS.$addedTextFile, 'text_'.$itext.'.txt');
	    									$toDelete[] = $this->_userPath.DS.$addedTextFile;
	    								}
	    							}
    							}
    						}
    					}
    				}
    			}
    			$zip->close();
    			if ($toDelete) {
    				foreach ($toDelete as $_del) {
    					unlink($_del);
    				}
    			}
    			return $this->getUploadUrl().$filename;
	    	}
	    	if ($opt['type'] == 'UPLOAD') {
	    		$image = $opt['image'];
    			$zip->addFile($this->_userPath.DS.$image, $image);
	    		$zip->close();
	    		return $this->getUploadUrl().$filename;
	    	}
	   }

	    return false;
    }

    public function getFinitionsList()
    {
        $list = array();
        $pattern = Mage::getModel('onio_custom/pattern')->getCollection();
        if ($pattern) {
            foreach ($pattern as $_pattern) {
                $list[$_pattern->getOptionId()] = $_pattern->getImage();
            }
        }
        return $list;
    }
    
}