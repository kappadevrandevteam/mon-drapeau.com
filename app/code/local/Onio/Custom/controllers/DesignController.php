<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_DesignController extends Mage_Core_Controller_Front_Action
{

    const MAX_UPL_SIZE = 2100000;

	public function preDispatch()
    {
        parent::preDispatch();
        
        // if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {}
        // else {
        //     $this->norouteAction();
        // }
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function productdesignerAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function canvaserrorAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function instagramAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function helpAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function viewAction()
    {
        $file = $this->getRequest()->getParam('file');
        if ($file) {
            $_helper = Mage::helper('onio_custom');
            $uploadPath = $_helper->getUploadPath();
            $fullFile = $this->getRequest()->getParam('full');
            if ($fullFile) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-image');
                echo file_get_contents($uploadPath.DS.$file);    
            }
            else {
                header("Content-disposition: attachment; filename=$file.png");
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-image');
                echo file_get_contents($uploadPath.DS.$file.'.png');    
            }
            
        }
        else {
            $this->norouteAction();
            return;
        }
    }

    public function saveAction() {
        $_helper = Mage::helper('onio_custom');
        $_hasError = false;

        $uploadPath = $_helper->getUploadPath();
        $uploadUrl = $_helper->getUploadUrl();

        $base64Str = substr($_POST['baseImage'], strpos($_POST['baseImage'], ",")+1);
        $cFlag = $_POST['cFlag'] ? $_POST['cFlag'] : uniqid();
        $btnId = $_POST['btnId'] ? $_POST['btnId'] : false;
        $decoded = base64_decode($base64Str);
        $pngName = $cFlag.'.png';
        $result = file_put_contents($uploadPath . DS .$pngName, $decoded);
        if($result) {
            $finalUrl = $btnId == 'save_btn' ? Mage::getUrl('custom/design/view', array('file' => $cFlag)) : $pngName;
            echo json_encode(array('success' => $finalUrl));
            exit();
        }
        else {
            echo json_encode(array('errors' => $_helper->__('Erreur lors de l\'enregistrement de votre fichier. Merci de réessayer')));
            exit();
        }
    }

    protected function _transformUploadSingle($file) {
        return $file;
    }

    public function uploadAction() {
        $_helper = Mage::helper('onio_custom');
        $_hasError = false;
        $keyFile = 'file';
        $editorContext = false;
        $keyError = 'errors';
        if (isset($_POST['uploadsDir']) && $_POST['uploadsDir'] == 'EDITOR') {
            $editorContext = true;
            $keyFile = 'images';
            $keyError = 'error';
            //$_FILES = $this->_transformUploadSingle($_FILES);

        }

        // Upload
        if(isset($_FILES[$keyFile]['name']) && $_FILES[$keyFile]['name'] != '') {

            if ($editorContext) {
                $allFiles = $_FILES;
                $countImg = count($_FILES[$keyFile]['name']);
                $_FILES = array($keyFile => array());
                for($i = 1; $i <= $countImg; $i++) {
                    $_FILES[$keyFile][$i-1] = array(
                        'name' => $allFiles[$keyFile]['name'][$i-1],
                        'type' => $allFiles[$keyFile]['type'][$i-1],
                        'tmp_name' => $allFiles[$keyFile]['tmp_name'][$i-1],
                        'error' => $allFiles[$keyFile]['error'][$i-1],
                        'size' => $allFiles[$keyFile]['size'][$i-1]
                    );
                }
            }
            else {
                $_FILES = array($keyFile => array($_FILES[$keyFile]));
            }

            $uploadPath = $_helper->getUploadPath();
            $uploadUrl = $_helper->getUploadUrl();

            foreach ($_FILES[$keyFile] as $k => $_file) {
                if ($_FILES[$keyFile][$k]['error'] == 1) {
                    $_hasError = $_helper->__('Erreur lors de l\'envoi de votre fichier.');
                    break;
                }
                if ($_FILES[$keyFile][$k]['size'] > self::MAX_UPL_SIZE) {
                    $_hasError = $_helper->__('Votre fichier ne doit pas dépasser 2Mo');
                    break;
                }
                try {
                    $uploader = new Varien_File_Uploader($_FILES[$keyFile][$k]);
                    $uploader->setAllowedExtensions(array('pdf','jpg','jpeg','ai','eps'));
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(false);
                    $ext = pathinfo($_FILES[$keyFile][$k]['name'], PATHINFO_EXTENSION);
                    $finalFileName = uniqid();
                    $finalFileName = $finalFileName.'.'.$ext;

                    $uploader->save($uploadPath, $finalFileName);
                    $userFile = $finalFileName;

                } catch(Exception $e) {
                    $_hasError = $_helper->__('Erreur lors de l\'envoi de votre fichier. Veuillez vérifier la taille, ainsi que le format.');
                    break;
                }
            }
        }

        if ($_hasError) {
            echo json_encode(array($keyError => $_hasError));
            exit();
        }
        if ($userFile) {
            echo $editorContext ? json_encode(array('image_src' => $uploadUrl.$userFile)) : json_encode(array('success' => $userFile));
            exit();
        }
        else {
            echo json_encode(array($keyError => $_helper->__('Erreur lors de l\'envoi de votre fichier. Merci de réessayer')));
            exit();
        }
        
    }

}