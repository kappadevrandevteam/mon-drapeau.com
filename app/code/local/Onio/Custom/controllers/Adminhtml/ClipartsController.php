<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Adminhtml_ClipartsController extends Mage_Adminhtml_Controller_action
{

	protected function _isAllowed()
	{
    	return Mage::getSingleton('admin/session')->isAllowed('catalog/onio_custom');
	}

	/**
     * Initialisation
     *
     */
	protected function _initAction()
	{
		$this->loadLayout()
			->_setActiveMenu('catalog')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		return $this;
	}
	/**
     * Render index
     *
     */
	public function indexAction()
	{
		$this->_initAction()->renderLayout();
	}

	public function editAction()
	{
		$id = $this->getRequest()->getParam('id');
		$model = Mage::getModel('onio_custom/cliparts')->load($id);
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			Mage::register('cliparts_data', $model);
			$this->loadLayout();
			$this->_setActiveMenu('catalog/items');
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->_addContent($this->getLayout()->createBlock('onio_custom/adminhtml_cliparts_edit'))->_addLeft($this->getLayout()->createBlock('onio_custom/adminhtml_cliparts_edit_tabs'));
			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('onio_custom')->__('Ce clipart est introuvable'));
			$this->_redirect('*/*/');
		}
	}

	public function newAction()
	{
		$this->_forward('edit');
	}

	/**
     * Save bloc
     *
     */
	public function saveAction()
	{
		if ($data = $this->getRequest()->getPost()) {
			$helper = Mage::helper('onio_custom');
			$model = Mage::getModel('onio_custom/cliparts');

			/* UPLOAD */
			// Check file folder
			$filesConfig = array();
			
			if(isset($data['set']) && isset($_FILES['set'])) {
				$i = 0;
				foreach ((array)$data['set'] as $kf => $_file) {
					$filesConfig[$i] = isset($_file['ffile_default']) ? $_file['ffile_default'] : false;
					$i++;
				}
				
				if(isset($_FILES['set'])) {
					$uplPath = Mage::helper('onio_custom')->getFileDirPath() . DS ;
					// Implement Data
					$jf = 0;
					foreach ($_FILES['set']['name'] as $kfile => $_f) {
						if(array_key_exists($jf, $filesConfig) && $filesConfig[$jf] == false) {
							if(!$_FILES['set']['name'][$kfile]["ffile"]) {
								unset($filesConfig[$jf]);
								$jf++;
								continue;
							}
							// Upload
							try {
								$uploader = new Varien_File_Uploader(
									array(
										'name' => $_FILES['set']['name'][$kfile]["ffile"],
										'type' => $_FILES['set']['type'][$kfile]["ffile"],
										'tmp_name' => $_FILES['set']['tmp_name'][$kfile]["ffile"],
										'error' => $_FILES['set']['error'][$kfile]["ffile"],
										'size' => $_FILES['set']['size'][$kfile]["ffile"]
									)
								);
								$uploader->setAllowRenameFiles(true);
								$uploader->setFilesDispersion(false);
								$savedFile = $uploader->save($uplPath, $_FILES['set']['name'][$kfile]["ffile"]);
								// Name
								$filesConfig[$jf] = $savedFile['file'];
								$jf++;
							} catch(Exception $e) {
								Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
							}
						}
						else
							$jf++;
					}
				}
			}

			$data['set'] = serialize($filesConfig);

			$model->setData($data)->setId($this->getRequest()->getParam('id'));

			try {
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('onio_custom')->__('Clipart enregistré avec succès!'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('onio_custom')->__('Enregistrement impossible'));
		$this->_redirect('*/*/');
	}

	/**
     * Delete bloc
     *
     */
	public function deleteAction()
	{
		if ($this->getRequest()->getParam('id') > 0) {
			try {
				$model = Mage::getModel('onio_custom/cliparts');

				$model->setId($this->getRequest()->getParam('id'))->delete();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('onio_custom')->__('La bibliothèque a été supprimée avec succès'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

}
