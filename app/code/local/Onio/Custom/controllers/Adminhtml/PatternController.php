<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Adminhtml_PatternController extends Mage_Adminhtml_Controller_action
{

	protected function _isAllowed()
	{
    	return Mage::getSingleton('admin/session')->isAllowed('catalog/onio_custom');
	}

	/**
     * Initialisation
     *
     */
	protected function _initAction()
	{
		$this->loadLayout()
			->_setActiveMenu('catalog')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		return $this;
	}
	/**
     * Render index
     *
     */
	public function indexAction()
	{
		$this->_initAction()->renderLayout();
	}

	public function editAction()
	{
		$id = $this->getRequest()->getParam('option_id');
		$model = Mage::getModel('onio_custom/pattern')->load($id);
		if ($model->getOptionId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			Mage::register('pattern_data', $model);
			$this->loadLayout();
			$this->_setActiveMenu('catalog/items');
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->_addContent($this->getLayout()->createBlock('onio_custom/adminhtml_pattern_edit'))->_addLeft($this->getLayout()->createBlock('onio_custom/adminhtml_pattern_edit_tabs'));
			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('onio_custom')->__('Cette finition est introuvable'));
			$this->_redirect('*/*/');
		}
	}

	/**
     * Save bloc
     *
     */
	public function saveAction()
	{
		if ($data = $this->getRequest()->getPost()) {
			$helper = Mage::helper('onio_custom');
			$model = Mage::getModel('onio_custom/pattern');

			$model->setData($data)->setId($this->getRequest()->getParam('option_id'));

			try {
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('onio_custom')->__('La finition a été enregistrée avec succès!'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('option_id' => $model->getOptionId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('option_id' => $this->getRequest()->getParam('option_id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('onio_custom')->__('Enregistrement impossible'));
		$this->_redirect('*/*/');
	}

}
