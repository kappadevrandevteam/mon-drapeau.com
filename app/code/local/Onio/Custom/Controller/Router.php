<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
	/**
	* Initialize Controller Router
	*
	* @param Varien_Event_Observer $observer
	*/
	public function initControllerRouters($observer)
	{
		/* @var $front Mage_Core_Controller_Varien_Front */
		$front = $observer->getEvent()->getFront();
		$front->addRouter('onio_catalog',$this);

	}

	public function match(Zend_Controller_Request_Http $request)
	{
		$urlKey = trim($request->getPathInfo(), '/');
        $match = false;
        // Check Product Designer Cases
        if ($urlKey == 'productdesigner.html') {
            $request->setModuleName('custom')
                ->setControllerName('design')
                ->setActionName('productdesigner');
            $match = true;
        }
        elseif($urlKey == 'instagram_auth.html') {
            $request->setModuleName('custom')
                ->setControllerName('design')
                ->setActionName('instagram');
            $match = true;
        }
        elseif($urlKey == 'canvaserror.html') {
            $request->setModuleName('custom')
                ->setControllerName('design')
                ->setActionName('canvaserror');
            $match = true;
        }
        elseif($urlKey == 'custom/upload') {
            $request->setModuleName('custom')
                ->setControllerName('design')
                ->setActionName('upload');
            $match = true;
        }

        return $match;
	}
}
