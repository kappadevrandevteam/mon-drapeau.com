<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Model_Cliparts extends Mage_Core_Model_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('onio_custom/cliparts');
	}	

	public function getItems()
	{
		$set = $this->getData('set');
		$set = Mage::helper('oniobase')->maybeUnserialize($set);
		$items = array();
		if ($set) {
			$path = Mage::helper('onio_custom')->getFileDirUrl();
			foreach ($set as $_item) {
				$items[] = $path.$_item;
			}
		}
		return $items;
	}

}
