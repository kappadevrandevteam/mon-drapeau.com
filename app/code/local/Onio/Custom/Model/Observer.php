<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Model_Observer
{
	public function attributeChanged(Varien_Event_Observer $observer)
    {
    	$request = Mage::app()->getRequest();
    	if ($request->getControllerName() == 'catalog_product_attribute' && $request->getActionName() == 'save') {
    		$params = Mage::app()->getRequest()->getParams();
    		$resourceModel = Mage::getResourceModel('onio_custom/pattern');
    		if (isset($params['attribute_id']) && $params['attribute_id'] == $resourceModel->getAttributeId()) {
    			 Mage::getResourceModel('onio_custom/pattern')->refreshFilters();
    		}
    	}
    }
}