<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Custom
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Custom_Model_Mysql4_Pattern extends Mage_Core_Model_Mysql4_Abstract
{

    protected $_attributeId; // Attribute Finition

	public function _construct()
	{
		$this->_init('onio_custom/pattern', 'option_id');
        $this->_isPkAutoIncrement = false;
        $this->_attributeId = Mage::helper('onio_custom')->getAttributeId();
        
	}

    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);
        
        $select->join(
            array('ov' => $this->getTable('eav/attribute_option_value')),
            $this->getTable('onio_custom/pattern').'.option_id=ov.option_id',
            array('value')
        );
        return $select;
    }

    public function getAttributeId()
    {
        return $this->_attributeId;
    }

    public function refreshFilters()
    {
        try {
            $this->purgeOrphan();
            $this->createMissingValues();

            $msg = Mage::helper('onio_custom')->__('Les options Finitions ont bien été mise à jour. Pensez à renseigner les images !');
            Mage::getSingleton('adminhtml/session')->addSuccess($msg);
        }
        catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }
    
    protected function purgeOrphan()
    {
        /** @var Magento_Db_Adapter_Pdo_Mysql $db */
        $db = $this->_getWriteAdapter();

        //remove options, removed from attributes
        $sqlIds = (string)$db->select()
            ->from(array('o' => $this->getTable('eav/attribute_option')), array('option_id'));
        $o = $this->getTable('onio_custom/pattern');
        $db->raw_query("DELETE FROM $o WHERE $o.option_id NOT IN(($sqlIds)) AND $o.option_id != 0");
    }

    protected function createMissingValues()
    {
        $db = $this->_getWriteAdapter();

        // create options
        $sql = $db->select()
            ->from(array('o' => $this->getTable('eav/attribute_option')), array())
            ->joinInner(array('ov' => $this->getTable('eav/attribute_option_value')), 'o.option_id = ov.option_id AND o.attribute_id = '.$this->_attributeId, array('ov.option_id'))
            ->joinLeft(array('v' => $this->getTable('onio_custom/pattern')), 'v.option_id = o.option_id', array())
            ->where('ov.store_id = 0')
            ->where('v.option_id IS NULL');

        $insertSql = 'INSERT INTO ' . $this->getTable('onio_custom/pattern') . '(option_id) ' .  (string)$sql;
        $db->raw_query($insertSql);
    }

}
