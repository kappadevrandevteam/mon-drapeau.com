<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Base
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Base_Block_Adminhtml_Editor extends Mage_Adminhtml_Block_Template
{
	protected function _prepareLayout()
	{
		if (Mage::app()->getRequest()->getParam('section') != 'recruitment' && Mage::app()->getRequest()->getParam('section') != 'onio_contacts') {
        	Mage::app()->getLayout()->getBlock('head')->addJs('onio/system.js');
    	}
    	else {
    		Mage::app()->getLayout()->getBlock('head')->addJs('onio/recruitment.js');	
    	}
		return parent::_prepareLayout();
	}
}