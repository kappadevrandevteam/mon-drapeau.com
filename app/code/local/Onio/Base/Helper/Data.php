<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Base
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Base_Helper_Data extends Mage_Core_Helper_Abstract
{

	public function getCollectionDatas($module, $model, $array = false, $options = array())
    {
    	// Define Options
    	$options = is_array($options) ? $options : array();
    	// Require Base Collection
    	$collection = Mage::getResourceModel($module.'/'.$model.'_collection');
    	// Sort Condition
    	if (isset($options['order']) && $options['order'] != "")
    	{
    		$dir = isset($options['dir']) && $options['dir'] != "" ? $options['dir'] : 'asc';
    		$collection->setOrder($options['order'], $dir);	
    	}
    	// Group Condition
    	if (isset($options['group']) && $options['group'] != "")
    	{
    		$collection->getSelect()->group($options['order']);
    	}
    	$list = array();
    	if ($collection)
    	{
    		if ( (!isset($options['fields'])) || 
    			(isset($options['fields']) && !$options['fields']) || 
    			(isset($options['fields']) && !is_array($options['fields']))
    		) {
    			$fields = array(
    				'id' => 'name'
    			);
    		}
    		else
    		{
    			$fields = $options['fields'];	
    		}
    		// Parse Collection
			foreach ($collection as $_item)
			{
				if ($fields)
				{
					foreach ($fields as $key => $value)
					{
						$list[$_item->getData($key)] = $_item->getData($value);
					}
				}
			}
		}
		if ($array)
    	{
    		return $this->toOptionArray($list);
    	}
		return $list;
    }

    public function toOptionArray($array)
    {
    	$list = array(
			array(
                'label' => '',
                'value' => ''
            )
		);
        foreach ($array as $key => $value) {
            $list[] = (array(
                'label' => (string)$value,
                'value' => $key
            ));
        }
        return $list;
    }


    public function maybeUnserialize( $data )
    {
        if ( $this->__isSerialized( $data ) ) // don't attempt to unserialize data that wasn't serialized going in
            return @unserialize( $data );
        return $data;
    }

    protected function __isSerialized( $data, $strict = true )
    {
        // if it isn't a string, it isn't serialized.
        if ( ! is_string( $data ) ) {
            return false;
        }
        $data = trim( $data );
        if ( 'N;' == $data ) {
            return true;
        }
        if ( strlen( $data ) < 4 ) {
            return false;
        }
        if ( ':' !== $data[1] ) {
            return false;
        }
        if ( $strict ) {
            $lastc = substr( $data, -1 );
            if ( ';' !== $lastc && '}' !== $lastc ) {
                return false;
            }
        } else {
            $semicolon = strpos( $data, ';' );
            $brace     = strpos( $data, '}' );
            // Either ; or } must exist.
            if ( false === $semicolon && false === $brace )
                return false;
            // But neither must be in the first X characters.
            if ( false !== $semicolon && $semicolon < 3 )
                return false;
            if ( false !== $brace && $brace < 4 )
                return false;
        }
        $token = $data[0];
        switch ( $token ) {
            case 's' :
                if ( $strict ) {
                    if ( '"' !== substr( $data, -2, 1 ) ) {
                        return false;
                    }
                } elseif ( false === strpos( $data, '"' ) ) {
                    return false;
                }
                // or else fall through
            case 'a' :
            case 'O' :
                return (bool) preg_match( "/^{$token}:[0-9]+:/s", $data );
            case 'b' :
            case 'i' :
            case 'd' :
                $end = $strict ? '$' : '';
                return (bool) preg_match( "/^{$token}:[0-9.E-]+;$end/", $data );
        }
        return false;
    }

    public function priceFormat($price, $clean = false)
    {
        $quote = Mage::helper('checkout')->getQuote();
        return $quote->getStore()->formatPrice($price, false, true, $clean);
    }

    public function searchInArray($array, $search, $keys = array()) {
        foreach($array as $key => $value) {
            if (is_array($value)) {
                $sub = $this->searchInArray($value, $search, array_merge($keys, array($key)));
                if (count($sub)) {
                    return $sub;
                }
            } elseif ($value === $search) {
                return array_merge($keys, array($key));
            }
        }
        return array();
    }

    public function wrapTitle($str) {
        $str = trim($str);
        $wordarray = explode(' ', $str);
        if (count($wordarray) > 1 ) {
            return preg_replace('/[^ ]*$/', '<span>$0</span>', $str);
        }
        return $str;
    }
    
}