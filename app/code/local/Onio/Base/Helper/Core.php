<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Base
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

// class Onio_Base_Helper_Core extends Mage_Core_Helper_Data
class Onio_Base_Helper_Core extends Inchoo_PHP7_Helper_Data
{

    /**
     * Formats price
     *
     * @param float $price
     * @param bool $includeContainer
     * @return string
     */
    public function formatPrice($price, $includeContainer = true)
    {
        $str = Mage::app()->getStore()->formatPrice($price, $includeContainer);
        return $this->separateformatPrice($str);
    }

    public function priceFormat($var)
    {
        return $this->separateformatPrice($var);

        $var = html_entity_decode($var);
        $pattern = '/[,](.*)[€]/u';
        //$symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
        return preg_replace($pattern,",$1<small>&euro;</small>",$var);
    }

    /**
     * Formats price separate
     *
     * @param float $price
     * @return string formated
     */
    public function separateformatPrice($price, $includeContainer = true)
    {
        // $price = Mage::app()->getStore()->formatPrice($price, $includeContainer);
        $proForm = array();
        $num_price = Mage::app()->getLocale()->getNumber($price);
        $pri = explode(',',$price);

        $proForm[0]=$pri[0];
        $proForm[1]= "<span class=\"cent\">,";
        if (!isset($pri[1])) $pri[1] = 0;
        // if(isset($pri[1]) && $pri[1]!=0):
        $proForm[2] = str_pad((int)$pri[1],2,"0",STR_PAD_RIGHT);
        // else:
            // $proForm[2] = "00";
        // endif;
        if ($includeContainer):
            $proForm[3]= '</span><sup class="d-sign">'.Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().'</sup>';
        else:
            $proForm[3] = '</span>';
        endif;
        $proForm[3] .= '</span>';
        $str = implode('',$proForm);
        return implode('',$proForm);
    }
    
}