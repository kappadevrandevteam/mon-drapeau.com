<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Base
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */
class Onio_Base_Model_Adminhtml_System_Config_Required extends Mage_Core_Model_Config_Data
{

	public function save()
	{
		if($this->getValue() == "")
		{
			$fieldConfig = $this->getFieldConfig();
			$label = (string)$fieldConfig->label;
			$label = strip_tags($label);
			$errorMsg = Mage::helper('oniobase')->__(sprintf('Le champ %s est obligatoire', $label));
			Mage::throwException($errorMsg);
		}
		return parent::save();
	}
}
