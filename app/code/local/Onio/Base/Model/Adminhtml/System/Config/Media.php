<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Base
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */
class Onio_Base_Model_Adminhtml_System_Config_Media extends Mage_Core_Model_Config_Data
{
	public function save()
	{
		if($this->getValue() == "")
		{
			Mage::throwException(Mage::helper('oniobase')->__('Vous devez renseigner une image')); 
		}

		$allowedExtensions = array('jpg', 'jpeg', 'png', 'gif', 'svg');
		$fileExtension = pathinfo($this->getValue(), PATHINFO_EXTENSION);

		if(!in_array($fileExtension, $allowedExtensions))
		{
			Mage::throwException(Mage::helper('oniobase')->__('Vous devez renseigner une image au format JPG, ou PNG')); 
		}

		return parent::save();
	}
}
