<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Base
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */
class Onio_Base_Model_Adminhtml_System_Config_Url extends Mage_Core_Model_Config_Data
{

	public function save()
	{
		$value = (string) $this->getValue();
		if($value == "")
		{
			$fieldConfig = $this->getFieldConfig();
			$label = (string)$fieldConfig->label;
			$label = strip_tags($label);
			$errorMsg = Mage::helper('oniobase')->__(sprintf('Le champ %s est obligatoire', $label));
			Mage::throwException($errorMsg);
		}

		$urlPattern = "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";
		if ( !preg_match($urlPattern, $value) )
		{
			$fieldConfig = $this->getFieldConfig();
			$label = (string)$fieldConfig->label;
			$label = strip_tags($label);
			$errorMsg = Mage::helper('oniobase')->__(sprintf('Le champ %s ne semble pas être une URL valide', $label));
			Mage::throwException($errorMsg);
		}
		return parent::save();
	}
}
