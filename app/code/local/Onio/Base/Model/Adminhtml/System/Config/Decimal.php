<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Base
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */
class Onio_Base_Model_Adminhtml_System_Config_Decimal extends Mage_Core_Model_Config_Data
{

	public function save()
	{
		$value = $this->getValue();
		$value = $value && $value != "" ? $value : 0;
		$value = is_float($value) ?: (float)$value;
		$this->setValue($value);
		return parent::save();
	}
}
