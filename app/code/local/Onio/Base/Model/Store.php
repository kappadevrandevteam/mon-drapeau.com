<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Base
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Base_Model_Store extends Mage_Core_Model_Store
{

    /**
     * Format price with currency filter (taking rate into consideration)
     *
     * @param   double $price
     * @param   bool $includeContainer
     * @return  string
     */
    public function formatPrice($price, $includeContainer = true, $totals = false, $clear = false)
    {
        if ($this->getCurrentCurrency()) {
            if ($totals) {
                return $this->separateformatPrice($price, $includeContainer, $clear);
            }
            return $this->getCurrentCurrency()->format($price, array(), $includeContainer);
        }
        return $price;
    }

    /**
     * Formats price separate
     *
     * @param float $price
     * @return string formated
     */
    public function separateformatPrice($price, $includeContainer = true, $clear = false)
    {
        $currency = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
        $ocr = explode($currency, $price);
        $proForm = $ocr[0];
        $proForm = trim($proForm);
        $proForm = number_format($proForm, 2, ",", " ");
        if (!$clear) {
            $proForm = "<span>$proForm</span>";
            $proForm .= " ".$currency;
        }
        else {
            $proForm = "<strong>$proForm";
            $proForm .= " ".$currency."</strong>";
        }
        
        return $proForm;
    }
    
}