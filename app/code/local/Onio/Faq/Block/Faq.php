<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Faq
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Faq_Block_Faq extends Mage_Core_Block_Template
{

	protected function _prepareLayout()
    {
    	$head = $this->getLayout()->getBlock('head');

        if ($head) {
            $head->setTitle($this->__('Foire aux Questions : Mon-drapeau.com'));
        }

        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
        	$breadcrumbs->addCrumb(
                'home',
                array(
                    'label' => $this->__('Accueil'),
                    'title' => $this->__('Accueil'),
                    'link'  => Mage::getBaseUrl(),
                )
            );
            $breadcrumbs->addCrumb(
                    'faq',
                array(
                    'label' => $this->__('FAQ'),
                    'title' => $this->__('FAQ')
                )
            );
        }
    }

    public function getFaq() {
    	$faq = Mage::getModel('onio_faq/faq')->getCollection();
    	return $faq;
    }

}