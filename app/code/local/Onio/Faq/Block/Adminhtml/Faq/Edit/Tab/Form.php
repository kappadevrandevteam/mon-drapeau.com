<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Faq
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Faq_Block_Adminhtml_Faq_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	
	protected function _prepareForm() {
		$form = new Varien_Data_Form();
		$this->setForm($form);

		$formDatas = new Varien_Object();

		if ( Mage::getSingleton('adminhtml/session')->getFaqData() ) {
			$formDatas = Mage::getSingleton('adminhtml/session')->getFaqData();
			Mage::getSingleton('adminhtml/session')->setFaqData(null);
		}
		else if ( Mage::registry('faq_data') ) {
			$formDatas = Mage::registry('faq_data');
		}
		
		$fieldset = $form->addFieldset('faq_form', array('legend' => Mage::helper('onio_faq')->__('Général')));

		$fieldset->addField('question', 'text', array(
			'label' => Mage::helper('onio_faq')->__('Question'),
			'title'  => Mage::helper('onio_faq')->__('Question'),
			'required' => true,
			'name' => 'question'
		));

		try {
            $config = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
            $config->setData(
                Mage::helper('blog')->recursiveReplace(
                    '/faq/',
                    '/' . (string)Mage::app()->getConfig()->getNode('admin/routers/adminhtml/args/frontName') . '/',
                    $config->getData()
                )
            );
        } catch (Exception $ex) {
            $config = null;
        }

		$fieldset->addField('response', 'editor', array(
			'label' => Mage::helper('onio_faq')->__('Réponse'),
			'title'  => Mage::helper('onio_faq')->__('Réponse'),
			'required' => true,
			'name' => 'response',
			'style'  => 'width:700px; height:500px;',
            'config' => $config
		));

		$form->setValues($formDatas);
		
		return parent::_prepareForm();
	}

}
