<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Customer
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Customer_Block_Account_Navigation extends Mage_Customer_Block_Account_Navigation
{
	public function removeLinkByName($name) {
		unset($this->_links[$name]);
		return $this;
	}

	public function addLink($name, $path, $label, $urlParams=array(), $icon = '')
    {
    	$urlParams = $urlParams ?: array();
    	
        $this->_links[$name] = new Varien_Object(array(
            'name' => $name,
            'path' => $path,
            'label' => $label,
            'icon' => $icon,
            'url' => $this->getUrl($path, $urlParams),
        ));
        return $this;
    }
}