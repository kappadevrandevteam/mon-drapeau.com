<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Menu
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Menu_Block_Menu extends Mage_Core_Block_Template
{

    protected $_helper;

    /**
     * Constructor. Set var, get config
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_helper = Mage::helper('onio_menu'); // Define helper
    }

    public function getMenuItems($menuId = 1)
    {
    	$_menu = Mage::getModel('onio_menu/menu')->load($menuId);
    	if ($_menu && $_menu->getId()) {
    		return $_menu->getElements();
    	}
    	return array();
    }

}
