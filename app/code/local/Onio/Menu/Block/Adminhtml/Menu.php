<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Menu
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Menu_Block_Adminhtml_Menu extends Mage_Adminhtml_Block_Widget_Grid_Container {

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_menu';
        $this->_blockGroup = 'onio_menu';
        $this->_headerText = Mage::helper('onio_menu') -> __('Gestion du menu');
        $this->_addButtonLabel = Mage::helper('onio_menu') -> __('Ajouter un menu');

        parent::__construct();

        $this->_removeButton('add');
    }

}
