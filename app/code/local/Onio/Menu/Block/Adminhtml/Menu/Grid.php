<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Menu
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Menu_Block_Adminhtml_Menu_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('menuGrid');
		$this->setDefaultSort('position');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('onio_menu/menu')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('id', array(
			'header' => Mage::helper('onio_menu')->__('ID'),
			'align' => 'right',
			'width' => '120px',
			'index' => 'id'
		));
		$this->addColumn('title', array(
			'header' => Mage::helper('onio_menu')->__('Titre'),
			'index' => 'title',
		));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}

}
