<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Menu
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Menu_Block_Adminhtml_Menu_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'onio_menu';
        $this->_controller = 'adminhtml_menu';
        
        $this->_updateButton('save', 'label', Mage::helper('onio_menu')->__('Enregistrer le lien'));
        $this->_updateButton('delete', 'label', Mage::helper('onio_menu')->__('Supprimer le lien'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__("Sauvegarder et continuer l'édition"),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('menu_data') && Mage::registry('menu_data')->getId() ) {
            return Mage::helper('onio_menu')->__('Edition du menu');
        } else {
            return Mage::helper('onio_menu')->__('Ajout d\'un menu');
        }
    }
}