<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Menu
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Menu_Block_Adminhtml_Menu_Edit_Tab_Elements extends Mage_Adminhtml_Block_Widget_Form
{
	
	protected function _prepareForm() {
		$form = new Varien_Data_Form();
		$this->setForm($form);

		$elementsFieldset = $form->addFieldset('menu_form_elements', array('legend' => Mage::helper('onio_menu')->__('Gestion des liens')));

		$field = $elementsFieldset->addField('elements', 'textarea',
            array(
                'label' => '',
                'name'  => 'elements',
            )
        );
        $field->setRenderer($this->getLayout()->createBlock('onio_menu/adminhtml_menu_renderer_elements'));

        if (Mage::getSingleton('adminhtml/session')->getMenuData()) {
			$datas = Mage::getSingleton('adminhtml/session')->getMenuData();
			Mage::getSingleton('adminhtml/session')->setMenuData(null);
		} elseif (Mage::registry('menu_data')) {
			$datas = Mage::registry('menu_data');
		}

		// Testing Serialized
		if (isset($datas['elements']) && $datas['elements'] != "") {
			if (is_array($datas['elements'])) {
				$datas['elements'] = @serialize($datas['elements']);
			}
		}
	
		$form->setValues($datas);
		
		return parent::_prepareForm();
	}

}
