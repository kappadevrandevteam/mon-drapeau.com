<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Menu
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Menu_Block_Adminhtml_Menu_Renderer_Elements extends Mage_Adminhtml_Block_Widget
    implements Varien_Data_Form_Element_Renderer_Interface
{
    /**
     * Object being rendered
     *
     * @var Varien_Data_Form_Element_Abstract
     */
    protected $_element = null;
    protected $_categoriesList = null;
    protected $_pageList = null;
    /**
     * Public constructor
     */
    public function __construct()
    {
        $this->setTemplate('onio/menu/elements.phtml');
    }

    /**
     * Renders html of block
     *
     * @param Varien_Data_Form_Element_Abstract $element
     *
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $this->_setAddButton();
        return $this->toHtml();
    }

    /**
     * Sets internal reference to element
     *
     * @param Varien_Data_Form_Element_Abstract $element
     *
     * @return Mage_Weee_Block_Renderer_Weee_Tax
     */
    public function setElement(Varien_Data_Form_Element_Abstract $element)
    {
        $this->_element = $element;
        return $this;
    }

    /**
     * Retrieves element
     *
     * @return Varien_Data_Form_Element_Abstract
     */
    public function getElement()
    {
        return $this->_element;
    }

    /**
     * Retrieves list of values
     *
     * @return array
     */
    public function getValues()
    {

        $values = array();
        $data = $this->getElement()->getValue();
        $data = @unserialize($data);
        if (is_array($data) && count($data)) {
            foreach ($data as $_data) {
                if (isset($_data['label'])) {
                    $values[] = array(
                        'label' => $_data['label'],
                        'link' => $_data['link'],
                        'important' => isset($_data['important']) ? $_data['important'] : 0,
                        'child' => isset($_data['child']) ? $_data['child'] : 0,
                    );
                }
            }
        }
        return $values;
    }

    public function getFileUrl($value = '')
    {
        return Mage::helper('onio_menu')->getFileUrl($value);
    }

    /**
     * Set add button and its properties
     */
    protected function _setAddButton()
    {
        $this->setChild('add_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array('id' => "add_element_" . $this->getElement()->getHtmlId(),
                'label' => Mage::helper('onio_menu')->__('Ajouter un lien'),
                'onclick' => "menuControl.addItem('" . $this->getElement()->getHtmlId() . "')",
                'class' => 'add'
            )));
    }

    /**
     * Retrieve add button html
     *
     * @return string
     */
    public function getAddButtonHtml()
    {
        return $this->getChildHtml('add_button');
    }

    public function getCategories()
    {
        if ($this->_categoriesList === null) {
            $_categories = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToFilter('level', array('gt' => 1))
                ->addAttributeToSelect('name');

            $_categories->getSelect()
                ->order(array('path asc', 'position asc'));

            foreach ($_categories as $_category) {
                $level = $_category->getLevel();
                $level = $level-1;
                $sep = '';
                $sep = str_repeat("-", $level);
                $sep = str_repeat("&nbsp;", $level).$sep;
                
                $this->_categoriesList[$_category->getEntityId()] = $sep.'&nbsp;'.$_category->getName();
            }
        }

        return $this->_categoriesList;
    }

    public function getPages()
    {

        if ($this->_pageList === null) {

            $_pages = Mage::getResourceModel('cms/page_tree')->load();
            $_pages = $_pages->getCollection()->addFieldToFilter('level', array('gt' => 1));

            foreach ($_pages as $_page) {
                if ($_page->getPageId() == 2) {
                    continue;
                }
                $level = $_page->getLevel();
                $level = $level-1;
                $sep = '';
                $sep = str_repeat("-", $level);
                $sep = str_repeat("&nbsp;", $level).$sep;
                
                $this->_pageList[$_page->getPageId()] = $sep.'&nbsp;'.$_page->getTitle();
            }
        }

        return $this->_pageList;
    }
}

