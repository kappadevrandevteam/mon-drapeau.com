<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Menu
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Menu_Block_Adminhtml_Menu_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('menu_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('onio_menu')->__('Informations'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('onio_menu')->__('Configuration'),
          'title'     => Mage::helper('onio_menu')->__('Configuration'),
          'content'   => $this->getLayout()->createBlock('onio_menu/adminhtml_menu_edit_tab_form')->toHtml(),
      ));

      $this->addTab('elements_section', array(
          'label'     => Mage::helper('onio_menu')->__('Liens'),
          'title'     => Mage::helper('onio_menu')->__('Liens'),
          'content'   => $this->getLayout()->createBlock('onio_menu/adminhtml_menu_edit_tab_elements')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}