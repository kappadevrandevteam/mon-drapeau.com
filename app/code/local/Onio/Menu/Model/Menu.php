<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Menu
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Menu_Model_Menu extends Mage_Core_Model_Abstract
{
	const TYPE_CATALOG      = 'category';
	const TYPE_CMS			= 'page';

	public function _construct()
	{
		parent::_construct();
		$this->_init('onio_menu/menu');
	}

	public function getElements()
	{
		$elements = $this->getData('elements');
		if ($elements && $elements != "") {
			$elements = Mage::helper('oniobase')->maybeUnserialize($elements);
			if (is_array($elements)) {

				$categoriesConfig = array();
				$pagesConfig = array();
				$categoriesId = array();
				$pagesId = array();
				$finalCategories = array();
				$finalPages = array();

				foreach ($elements as $_k => $_e) {
					if (!isset($_e['link'])) {
						unset($elements[$_k]);
						continue;
					}
					$link = $_e['link'];
					$linkType = explode('_', $link);
					$finalId = $linkType[1];
					$finalId = $finalId == 0 ? 2 : $finalId;
					$elements[$_k]['type'] = $linkType[0];
					$elements[$_k]['id'] = $finalId;
					unset($elements[$_k]['link']);

					if (count($linkType) == 2) {
						
						if ($linkType[0] == self::TYPE_CATALOG) {
							
							$categoriesConfig[$finalId] = (bool)$_e['child'];
							$categoriesId[$_k] = $finalId;
						}
						elseif ($linkType[0] == self::TYPE_CMS) {
							$pagesConfig[$finalId] = (bool)$_e['child'];
							$pagesId[$_k] = $finalId;
						}
					}
				}

				if ($categoriesId) {
					$catIds = array_values($categoriesId);
					$childsLoad = array();

					$categories = Mage::getModel('catalog/category')->getCollection()
						->addAttributeToSelect('name')
						->addAttributeToFilter('include_in_menu', 1)
						->addAttributeToFilter('entity_id', array('in' => $catIds));
					if ($categories) {
						foreach ($categories as $_category) {
							$_catId = $_category->getId();
							$withChild = $categoriesConfig[$_catId];
							$finalCategories[$_catId] = array(
								'url' => $_category->getUrl($_category)
							);
							if ($withChild) {
								$childs = $_category->getChildren();
								$childs = explode(',', $childs);
								$childsLoad[$_catId] = array_merge($childsLoad, $childs);
							}
						}
						if ($childsLoad) {
							$childCategories = Mage::getModel('catalog/category')->getCollection()
								->addAttributeToSelect('*')
								->addAttributeToFilter('include_in_menu', 1)
								->addAttributeToFilter('entity_id', array('in' => $childsLoad));
							foreach ($childCategories as $_child) {
								$_childId = $_child->getId();
								$path = $_child->getPath();
								$path = explode('/', $path);
								$parentId = $path[2];
								$finalCategories[$parentId]['childs'][$_childId] = array(
									'label' => $_child->getName(),
									'url' => $_child->getUrl($_child),
									'subchilds' => array()
								);
								$subChilds = $_child->getChildrenCategories();
								if ($subChilds) {
									foreach ($subChilds as $_sub) {
										$_subId = $_child->getId();
										$finalCategories[$parentId]['childs'][$_childId]['subchilds'][] = array(
											'label' => $_sub->getName(),
											'url' => $_sub->getUrl($_sub),
										);
									}
								}
							}
						}
					}
				}

				if ($pagesId) {
					$pageIds = array_values($pagesId);
					$childsLoad = array();


					$_pages = Mage::getResourceModel('cms/page_tree')->load();
		            $_pages = $_pages->getCollection()
		            	->addFieldToFilter('page_id', array('in' => $pageIds));

		            foreach ($_pages as $_page) {
		            	$pageId = $_page->getId();
		                $finalPages[$pageId] = array(
		                	'url' => Mage::helper('cms/page')->getPageUrl($pageId)
		                );
		            }
				}

				foreach ($elements as $_k => $_e) {
					$type = $_e['type'];
					if ($type == self::TYPE_CATALOG) {
						if (isset($finalCategories[$_e['id']])) {
							$elements[$_k]['tree'] = $finalCategories[$_e['id']];
						}
						
					}
					elseif ($type == self::TYPE_CMS) {
						if (isset($finalPages[$_e['id']])) {
							$elements[$_k]['tree'] = $finalPages[$_e['id']];
						}
					}
					unset($elements[$_k]['child']);
					unset($elements[$_k]['type']);
					unset($elements[$_k]['id']);
				}

				return $elements;
				$elementsObject = new Varien_Object();
	        	$elementsObject->setData($elements);
	        	return $elementsObject;
	        }
		}
		return false;
	}
}
