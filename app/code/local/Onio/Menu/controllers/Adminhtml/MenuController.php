<?php
/**
 * Onio.
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. Onio does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   Onio
 * @package    Menu
 * @version    1.0.0
 * @copyright  Copyright (c) 2017-2017 Onio. 
 */

class Onio_Menu_Adminhtml_MenuController extends Mage_Adminhtml_Controller_action
{

	protected function _isAllowed()
	{
    	return Mage::getSingleton('admin/session')->isAllowed('cms/onio_menu');
	}

	/**
     * Initialisation
     *
     */
	protected function _initAction()
	{
		$this->loadLayout()
			->_setActiveMenu('cms')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		return $this;
	}
	/**
     * Render index
     *
     */
	public function indexAction()
	{
		$this->_initAction()->renderLayout();
	}

	public function editAction()
	{
		$id = $this->getRequest()->getParam('id');
		$model = Mage::getModel('onio_menu/menu')->load($id);
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			Mage::register('menu_data', $model);
			$this->loadLayout();
			$this->_setActiveMenu('cms/items');
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->_addContent($this->getLayout()->createBlock('onio_menu/adminhtml_menu_edit'))->_addLeft($this->getLayout()->createBlock('onio_menu/adminhtml_menu_edit_tabs'));
			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('onio_menu')->__('Cet élément est introuvable'));
			$this->_redirect('*/*/');
		}
	}

	public function newAction()
	{
		$this->_forward('edit');
	}

	/**
     * Save bloc
     *
     */
	public function saveAction()
	{
		if ($data = $this->getRequest()->getPost()) {
			$helper = Mage::helper('onio_menu');
			$model = Mage::getModel('onio_menu/menu');


			foreach ($data['elements'] as $_elementK => $_elementData) {
				if ($_elementData['delete'] != "") {
					unset($data['elements'][$_elementK]);
					continue;
				}
				if (!isset($_elementData['child'])) {
					$data['elements'][$_elementK]['child'] = "0";
				}
				unset($data['elements'][$_elementK]['delete']);
			}
			$data['elements'] = @serialize($data['elements']);

			$model->setData($data)->setId($this->getRequest()->getParam('id'));

			try {
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('onio_menu')->__('Le menu a été enregistré avec succès!'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('onio_menu')->__('Enregistrement impossible'));
		$this->_redirect('*/*/');
	}

	/**
     * Delete bloc
     *
     */
	public function deleteAction()
	{
		if ($this->getRequest()->getParam('id') > 0) {
			try {
				$model = Mage::getModel('onio_menu/menu');

				$model->setId($this->getRequest()->getParam('id'))->delete();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('onio_menu')->__('le lien a été supprimé du menu'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

}
